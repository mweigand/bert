// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
using namespace DCFEMLib;

#include <longoptions.h>

#include <mesh2d.h>
#include <domain2d.h>

using namespace MyMesh;

#include <string>
using namespace std;

int main(int argc, char *argv[]){

  LongOptionsList lOpt;
  lOpt.setLastArg("poly-file");
  lOpt.setDescription( (string) "Create binary dcfemlib 2d mesh from poly file (wrapper for triangle)" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "smoothmesh", no_argument, 'S', "Try to increase the mesh quality by smoothing [ false ]" );
  lOpt.insert( "quality", required_argument, 'q', "Mesh quality, should be lower 35 [33 = regular]" );
  lOpt.insert( "area", required_argument, 'a', "Maximum triangle area [0.0]" );

  double quality = 33.0, maxArea = 0.0;

  bool verbose = false, smooth = false;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hv"
				       "S" // smooth mesh
				       "q:" // mesh quality
				       "a:" // max area;
				       , lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'S': smooth = true; break;
    case 'q': quality = atof(optarg); break;
    case 'a': maxArea = atof(optarg); break;
    default : cerr << WHERE_AM_I << " undefined option: " << (char)option_char << endl;
    }
  }
  if ( argc == 1 ) { lOpt.printHelp( argv[0] ); return 1;  }

  string baseFilename= string( argv[ argc -1 ] ).substr(0, string( argv[ argc -1 ] ).rfind(".") );
  string polyFilename( baseFilename + ".poly" );
  string meshFilename( baseFilename + MESHBINSUFFIX );

  if ( verbose ){
    cout << "polyfile: " << polyFilename << endl
	 << "meshfile: " << meshFilename << endl
	 << "mesh quality: " << quality << endl;
  }

  Domain2D poly( polyFilename );
  //  poly.save( "test.poly" );

  Mesh2D mesh( poly.createMesh( quality, maxArea, verbose ) );
  //  mesh.save( "test", Ascii );

  if ( smooth ){
    mesh.improveMeshQuality( true, true, 0, 2);
    //mesh.improveMeshQuality( true, true, 1, 4);
  }

  mesh.save( meshFilename );

  return 0;



}

/*
$Log: dctriangle.cpp,v $
Revision 1.9  2010/06/14 11:53:55  carsten
*** empty log message ***

Revision 1.8  2009/04/02 16:33:15  carsten
*** empty log message ***

Revision 1.7  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.6  2005/12/15 15:15:13  carsten
*** empty log message ***

Revision 1.5  2005/10/24 12:06:04  carsten
*** empty log message ***

Revision 1.4  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.3  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.2  2005/07/29 15:51:06  carsten
*** empty log message ***

Revision 1.1  2005/07/28 19:43:43  carsten
*** empty log message ***

*/
