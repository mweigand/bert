// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef ELECTRODECONFIG__H
#define ELECTRODECONFIG__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "datamap.h"

#include "myvec/vector.h"
using namespace MyVec;


#include "elements.h"
using namespace MyMesh;

#include "line.h"


#include <iostream>
#include <vector>

using namespace std;

// #include "mesh2d.h"
// #include "mesh3d.h"
// #include "vektor/vektor.h"
// #include "numfunct.h"
// #include "datamap.h"
// #include "line.h"

class Electrode : public RealPos {
public:
  Electrode( const RealPos & pos, int id ) : RealPos( pos ), id_ ( id ), used_( false ) { }

  ~Electrode( ){};
  Electrode( const Electrode & el ) : RealPos( el ){
    id_ = el.id();
    used_ = el.used();
  }
  Electrode & operator = ( const Electrode & el ){
    if ( this != & el ){
      setPos( el );
      id_ = el.id();
      used_ = el.used();
    }
    return *this;
  }

  void setPos( const RealPos & pos ){
    x_ = pos.x();
    y_ = pos.y();
    z_ = pos.z();
  }
  RealPos pos( ) const { return RealPos( x_, y_, z_ );  }

  int id( ) const { return id_; }
  void setId( int id ) { id_ = id; }

  bool used() const { return used_; }
  void setUsed( bool used ) { used_ = used; }

protected:
  int id_;
  bool used_;
};

class ElectrodeConfig{
public:
  ElectrodeConfig( int A, int M ) : a_( A ), m_( M ) {
    b_ = -1;
    n_= -1;
    setDefaults_();
  }

  ElectrodeConfig( int A, int B, int M, int N ) : a_( A ), b_( B ), m_( M ), n_( N ) {
    setDefaults_();
  }

  ElectrodeConfig( const ElectrodeConfig & eConf ){
    copyAllMember( eConf );
  }
  virtual ~ElectrodeConfig( ){}

  ElectrodeConfig & operator = ( const ElectrodeConfig & eConf ){
    if ( this != &eConf ){
      copyAllMember( eConf );
    }
    return *this;
  }

  virtual void copyAllMember( const ElectrodeConfig & eConf ){
    a_ = eConf.a(); b_ = eConf.b();
    m_ = eConf.m(); n_ = eConf.n();
    copyAllVals( eConf );
  }

  virtual void copyAllVals( const ElectrodeConfig & eConf ){
    rhoA_ = eConf.rhoA();
    current_ = eConf.i();
    deltaU_ = eConf.u();
    r_ = eConf.r();
    K_ = eConf.k();
    error_ = eConf.err();
    ip_ = eConf.ip();
    //    deltaURez_ = eConf.deltaURez();
    enable_ = eConf.isEnabled();
  }

  int a() const { return a_; }
  //  RealPos a( const BaseMesh & mesh ){ return mesh.node( a_ ).pos(); }
  int b() const { return b_; }
  // RealPos b( const BaseMesh & mesh ){ return mesh.node( b_ ).pos(); }
  int m() const { return m_; }
  // RealPos m( const BaseMesh & mesh ){ return mesh.node( m_ ).pos(); }
  int n() const { return n_; }
  // RealPos n( const BaseMesh & mesh ){ return mesh.node( n_ ).pos(); }

  double u() const { return deltaU_; }
  void setU( double deltaU ) { deltaU_ = deltaU; }
//   double deltaU() const { return deltaU_; }
//   void setDeltaU( double deltaU ) { deltaU_ = deltaU; }
//   void updateDeltaU() { deltaU_ = rhoA_ / K_; }

  double i() const { return current_; }
  void setI( double current ) { current_ = current; }
//   double current() const { return current_; }
//   void setCurrent( double current ) { current_ = current; }


  double err() const { return error_; }
  void setErr( double error ){ error_ = error; }
//   double error() const { return error_; }
//   void setError( double error ){ error_ = error; }


  double k() const { return K_; }
  void setK( double k ){ K_ = k; }
  //  void updateK(){ if (deltaU_== 0.0 ) K_= 0.0; else K_ = rhoA_ / deltaU_ ; }

  double r() const { return r_; }
  void setR( double r ){ r_ = r; }

  double t() const { return t_; }
  void setT( double t ){ t_ = t; }

  double ip() const { return ip_; }
  void setIp( double ip ){ ip_ = ip; }

  double rhoA() const { return rhoA_; }
  void setRhoA( double rhoA ) { rhoA_ = rhoA; }

  double uRez() const { return deltaURez_; }
  void setURez( double deltaU ) { deltaURez_ = deltaU; }

  void setEnabled( bool enable ){ enable_ = enable; }
  void disable( ) { enable_ = false; }
  bool isEnabled( ) const { return enable_; }

protected:
  void setDefaults_(){
    ip_ = 0.0;
    r_ = 0.0;
    t_ = 1.0;
    current_ = 1.0;
    rhoA_ = 0.0;
    deltaU_ = 0.0;
    deltaURez_ = 0.0;
    K_ = 0.0;
    error_ = 0.0;
    enable_ = true;
  }

  int a_, b_, m_, n_;
  double rhoA_;
  double current_;
  double deltaU_;
  double deltaURez_;
  double K_;
  double r_;
  double t_; // topoeffect
  double ip_;
  double error_;
  bool enable_;
};

ostream & operator << ( ostream & str, const ElectrodeConfig & p );

class DLLEXPORT ElectrodeConfigVector : public vector< ElectrodeConfig > {
public:
  ElectrodeConfigVector( bool verbose = false )
    : verbose_( verbose ), isBury_( false ) {
    this->clear();
    inputFormatStringElecs_ = "x y z";
  }

  virtual ~ElectrodeConfigVector( ){ }

  ElectrodeConfigVector( const ElectrodeConfigVector & eConfVect ) {
        copy_( eConfVect );
  }

  ElectrodeConfigVector & operator = ( const ElectrodeConfigVector & eConfVect ){
    if ( this != & eConfVect ){
        copy_( eConfVect );
    }
    return *this;
  }

    void copy_( const ElectrodeConfigVector & eConfVect ){
        setElectrodePositions( eConfVect.electrodePositions() );
        setElectrodeDepths( eConfVect.electrodeDepths() );

        for ( size_t i = 0; i < eConfVect.size(); i ++ ) this->push_back( eConfVect[ i ] );
        verbose_ = eConfVect.verbose();
        isBury_ = eConfVect.areBuryElectrodesPresent();
    }   

  int load( const string & fileName, const string & formatString, bool fromOne );
  int load( const string & fileName, bool fromOne ) { return load( fileName, "a b m n rhoa err ip u i", fromOne );}
  int load( const string & fileName ){ return load( fileName, "a b m n rhoa err ip u i", true );  }

  int save( const string & fileName, const string & elecString, const string & formatString, bool fromOne ) const;
  int save( const string & fileName, const string & formatString, bool fromOne ) const {
    return save( fileName, "x y z", formatString, true );
  }

  int save( const string & fileName, const string & formatString ) const { return save( fileName, "x y z", formatString, true ); }
  int save( const string & fileName, bool fromOne ) const { return save( fileName, "x y z", "a b m n rhoa", fromOne ); }
  int save( const string & fileName ) const { return save( fileName, "x y z", "a b m n rhoa", true ); }

  int readGeoTomFile( const string & fileName );
  int readELTKRNFile( const string & fileName, double radius = 10, bool verbose = false );

  DLLEXPORT RVector rhoA() const;
  DLLEXPORT void setRhoA( const RVector & vec );

  DLLEXPORT RVector u() const;
  DLLEXPORT void setU( const RVector & vec );

  DLLEXPORT RVector uRez() const;
  DLLEXPORT void setURez( const RVector & vec );

  DLLEXPORT RVector r() const;
  DLLEXPORT void setR( const RVector & vec );

  DLLEXPORT RVector i() const;
  DLLEXPORT void setI( const RVector & vec );

  DLLEXPORT RVector t() const;
  DLLEXPORT void setT( const RVector & vec );

  DLLEXPORT RVector err() const;
  DLLEXPORT void setErr( const RVector & vec );

  DLLEXPORT RVector ip() const;
  DLLEXPORT void setIp( const RVector & vec );

  DLLEXPORT RVector k() const;
  DLLEXPORT void setK( const RVector & vec );

  void calcK( SpaceConfigEnum config = HALFSPACE, double mirrorPlaneZ = 0.0 );

  void setElectrodeDepths( const RVector & d ){ depths_ = d; }
  const RVector & electrodeDepths( ) const { return depths_; }

  void setBuryElectrodesPresent( bool bury ){ isBury_ = bury; }
  bool areBuryElectrodesPresent( ) const { return isBury_; }

  size_t electrodeCount() const { return electrodePositions_.size(); }

  RealPos electrodePos( int i ){ return electrodePositions_[ i ]; }
  RealPos * pElectrodePos( int i ){ return & electrodePositions_[ i ]; }

    void setElectrodePositions( const vector < RealPos > & electrodePositions ){ electrodePositions_ = electrodePositions;  }

    const vector < RealPos > & electrodePositions() const { return electrodePositions_ ; }

    vector < RealPos > electrodePositions( bool withDepth ) const {
        vector < RealPos > tmp( electrodePositions_ );
        for ( uint i = 0; i < electrodePositions_.size(); i ++ ) {
            if ( (long)depths_.size() > i ) {
                tmp[ i ][ 2 ] -= depths_[ i ];
            }
        }
        return tmp;
    }

    void setElectrodePositions( const DataMap & dataMap );

    void addElectrode( const RealPos & ePos ){ electrodePositions_.push_back( ePos ); }

  void collectPotentials( const DataMap & map );

  void createDummyElectrodes();

  void merge( ElectrodeConfigVector & profile );
  void add( ElectrodeConfigVector & profile );

  RealPos posA( size_t i ) const { return electrodePositions_[ (*this)[ i ].a() ]; }
  RealPos posB( size_t i ) const { return electrodePositions_[ (*this)[ i ].b() ]; }
  RealPos posM( size_t i ) const { return electrodePositions_[ (*this)[ i ].m() ]; }
  RealPos posN( size_t i ) const { return electrodePositions_[ (*this)[ i ].n() ]; }

  Line line( size_t i ){
    return Line( electrodePositions_[ (*this)[ i ].a() ], electrodePositions_[ (*this)[ i ].b() ]);
  }

  bool verbose ( ) const { return verbose_; }
  void setVerbose ( bool verbose ) { verbose_ = verbose; }

  string inputFormatString() const { return inputFormatString_; }
  void setInputFormatString( const string & str ) { inputFormatString_ = str; }

  string inputFormatStringElecs() const { return inputFormatStringElecs_; }
  void setInputFormatStringElecs( const string & str ) { inputFormatStringElecs_ = str; }


protected:
  bool verbose_;
  bool isBury_;
//   int readGeoTomFileSchlumberger( fstream & file, double spacing, int nConfig );
//   int readGeoTomFileWenner( fstream & file, double spacing, int nConfig );
//   int readGeoTomFileDipolDipol( fstream & file, double spacing, int nConfig );

  RVector depths_;
  string inputFormatString_;
  string inputFormatStringElecs_;
  vector < RealPos > electrodePositions_;
};

class DLLEXPORT ElectrodeConfigSmart : public ElectrodeConfig{
public:
  ElectrodeConfigSmart( Electrode & eA, Electrode & eB, Electrode & eM, Electrode & eN )
    : ElectrodeConfig( eA.id(), eB.id(), eM.id(), eN.id() ),
      eA_( &eA ), eB_( &eB ), eM_( &eM ), eN_( &eN ){
    eA.setUsed( true ); eB.setUsed( true );
    eM.setUsed( true ); eN.setUsed( true );
  }
  ~ElectrodeConfigSmart(){}

  Electrode * eA() { return eA_; }
  Electrode * eB() { return eB_; }
  Electrode * eM() { return eM_; }
  Electrode * eN() { return eN_; }

  RealPos aPos() const { return *eA_; }
  RealPos bPos() const { return *eB_; }
  RealPos mPos() const { return *eM_; }
  RealPos nPos() const { return *eN_; }

  Electrode * electrode( int i ){
    if ( i < 0 || i > 3 ) cerr << WHERE_AM_I << i << endl;
    switch ( i ){
    case 0: return eA_;
    case 1: return eB_;
    case 2: return eM_;
    case 3: return eN_;
    }
    return 0;
  }

  bool containsIdx( const set < int > & profile ){
    int success = 0;
    for ( int i = 0; i < 4; i ++ ){
      if ( profile.find( electrode( i )->id() ) != profile.end() ){
	success++;
      }
    }
    return ( success > 1 );
  }

protected:
  Electrode * eA_;
  Electrode * eB_;
  Electrode * eM_;
  Electrode * eN_;
};

ostream & operator << ( ostream & str, ElectrodeConfigSmart & p );

class DLLEXPORT ElectrodeConfigSmartVector : public vector< ElectrodeConfigSmart * > {
public:
  ElectrodeConfigSmartVector( bool verbose = false )
    : vector< ElectrodeConfigSmart * >(), verbose_( verbose ) { }

  ~ElectrodeConfigSmartVector(){}

  ElectrodeConfigSmartVector( const ElectrodeConfigSmartVector & dataSet )
    : vector< ElectrodeConfigSmart * >() {
    //    cout << "start COPOP"  << endl;
    for ( size_t i = 0; i < dataSet.electrodeCount(); i ++){
      createElectrode( dataSet.electrode( i ) );
      //      cout << i << endl;
    }
    for ( size_t i = 0; i < dataSet.size(); i ++){
//       cout << dataSet[ i ]->eA()->id() <<  " " << dataSet[ i ]->eA() << " "
// 	   << " " << dataSet[ i ]->eB()->id()
// 	   << " " << dataSet[ i ]->eM()->id()
// 	   << " " << dataSet[ i ]->eN()->id() << endl;
      this->push_back( new ElectrodeConfigSmart( *electrodes_[ dataSet[ i ]->eA()->id() -1 ],
					     *electrodes_[ dataSet[ i ]->eB()->id() -1 ],
					     *electrodes_[ dataSet[ i ]->eM()->id() -1 ],
					     *electrodes_[ dataSet[ i ]->eN()->id() -1 ] ) );
      this->back()->copyAllVals( *dataSet[ i ] );
      verbose_ = dataSet.verbose();
    }
    //    cout << "end COPOP"  << endl;
  }

  ElectrodeConfigSmartVector & operator=( const ElectrodeConfigSmartVector & dataSet ) {
    //    cout << "start ="  << endl;
    if ( this != &dataSet ){
      this->clear(); electrodes_.clear();
      for ( size_t i = 0; i < dataSet.electrodeCount(); i ++){
	createElectrode( dataSet.electrode( i ) );
      }
      for ( size_t i = 0; i < dataSet.size(); i ++){
	this->push_back( new ElectrodeConfigSmart( *electrodes_[ dataSet[ i ]->eA()->id() -1 ],
						   *electrodes_[ dataSet[ i ]->eB()->id() -1 ],
						   *electrodes_[ dataSet[ i ]->eM()->id() -1 ],
						   *electrodes_[ dataSet[ i ]->eN()->id() -1 ] ) );
	this->back()->copyAllVals( *dataSet[ i ] );
      }
      verbose_ = dataSet.verbose();
    }
    //    cout << "end ="  << endl;
    return *this;
  }

  void createElectrode( const RealPos & pos, int id ){
    electrodes_.push_back( new Electrode( pos, id ) );
  }
  void createElectrode( const Electrode & elec ){
    createElectrode( elec.pos(), elec.id() );
  }
  vector < ElectrodeConfigSmartVector > profileLines();
  vector < RealPos > electrodePositions( );

  ElectrodeConfigSmartVector filterElectIdx( const vector < int > & idxVec );
  ElectrodeConfigSmartVector filterElectIdx( const set < int > & idxSet );
  void add( ElectrodeConfigSmartVector & profile );
  void add( ElectrodeConfigSmart & data );
  void renumberElectrodes();
  void reorderElectrodes();

  int load( const string & fileName, const string & formatString, bool fromOne, bool sortElecs );
  int load( const string & fileName, const string & formatString, bool fromOne ){
   return load( fileName, formatString, fromOne, false );
  }
  int save( const string & fileName, const string & formatString, bool fromOne );
  int save( const string & fileName, const string & formatString ){ return save( fileName, formatString, true );}


  int load( const string & fileName, bool fromOne = true ){
    return load( fileName, "a b m n rhoa err ip u i", fromOne );
  }
  int save( const string & fileName, bool fromOne = true ){
    return save( fileName, "a b m n rhoa", fromOne );
  }

  size_t electrodeCount() const { return electrodes_.size(); }

  Electrode & electrode( size_t i ){ return *electrodes_[ i ]; }
  Electrode electrode( size_t i ) const { return *electrodes_[ i ]; }

  int swapYZ( );
  int scale( const RealPos & start, const RealPos & end );
  int translate( const RealPos & trans );

  RealPos posA( size_t i ) const { return (*this)[ i ]->eA()->pos(); }
  RealPos posB( size_t i ) const { return (*this)[ i ]->eB()->pos(); }
  RealPos posM( size_t i ) const { return (*this)[ i ]->eM()->pos(); }
  RealPos posN( size_t i ) const { return (*this)[ i ]->eN()->pos(); }

  int saveEltomoFile( const string & fileName );

  void setAllRhoA( const RVector & allRhoA );
  RVector allRhoA() const;

  bool verbose ( ) const { return verbose_; }
  void setVerbose ( bool verbose ) { verbose_ = verbose; }

  string inputFormatString() const { return inputFormatString_; }

protected:
  bool verbose_;
  vector < Electrode * > electrodes_;
  string inputFormatString_;
};

DLLEXPORT int loadElectrodeList( vector < RealPos > & electrodeList, const string & fname );


// class Datum{

// }

// class DataList{
// }

#endif //ELECTRODECONFIG__H

/*
$Log: electrodeconfig.h,v $
Revision 1.25  2009/04/21 21:19:57  carsten
*** empty log message ***

Revision 1.24  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.23  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.22  2006/10/10 09:46:01  carsten
*** empty log message ***

Revision 1.21  2006/07/20 16:05:47  carsten
*** empty log message ***

Revision 1.20  2006/04/05 19:00:13  carsten
*** empty log message ***

Revision 1.19  2006/02/25 18:46:44  carsten
*** empty log message ***

Revision 1.17  2006/01/29 17:54:05  carsten
*** empty log message ***

Revision 1.16  2006/01/26 18:24:06  carsten
*** empty log message ***

Revision 1.15  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.14  2005/07/13 13:36:49  carsten
*** empty log message ***

Revision 1.13  2005/07/01 15:11:28  carsten
*** empty log message ***

Revision 1.12  2005/06/30 17:32:16  carsten
*** empty log message ***

Revision 1.11  2005/06/27 09:28:55  carsten
*** empty log message ***

Revision 1.10  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.9  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.8  2005/04/11 13:12:35  carsten
*** empty log message ***

Revision 1.7  2005/03/18 19:47:17  carsten
*** empty log message ***

Revision 1.6  2005/03/17 18:17:13  carsten
*** empty log message ***

Revision 1.5  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.4  2005/01/12 21:06:45  carsten
*** empty log message ***

Revision 1.3  2005/01/06 14:24:52  carsten
*** empty log message ***

Revision 1.2  2005/01/06 14:19:56  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
