// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef LINE__H
#define LINE__H LINE__H

#include "elements.h"

using namespace std;

namespace MyMesh{

// A straight line in the geometrically point of view.
/*! */
class DLLEXPORT Line{
public:
  /*! Default constructor. Constructs an invalid straight line. \n
    An example ( found in tests/meshcreate/intersection ):\n
    The intersection of two planes is a straightline. If the planes are parallel the straightline is invalid.\n
    ...\n
    success.push_back( compare( false, plane1.intersect( plane2 ).valid(), verbose ) );\n
    ...\n
  */
  Line( );
  /*! */
  Line( const RealPos & p0 );
  /*! */
  Line( const RealPos & p0, const RealPos & p1 );
  /*! */
  Line( const Edge & edge );
  /*! */
  Line( const Line & line );
  /*! Default destructor. Destroys the object.*/
  ~Line();
  /*! */
  Line & operator = ( const Line & line );
  
  /*! */
  bool operator == ( const Line & line );
  /*! */
  bool operator != ( const Line & line );
  /*! */
  void setPositions( const RealPos & p0, const RealPos & p1 );
  /*! */
  RealPos p0( ) const { return p0_; }
  /*! */
  RealPos p1( ) const { return p1_; }
  
  /*! */
  RealPos center() const { return ( p0_ + p1_) / 2.0 ; }
  /*! */
  RealPos intersect( const Line & line) const;
  /*! */
  RealPos lineAt( double t ){ return p0_ + ( p1_ - p0_ ) * t; }

  RealPos norm() const { TO_IMPL return RealPos(); }

  double distance( const RealPos & pos ) const;
  double t( const RealPos & pos, double tol = TOLERANCE ) const ;

  /*! Returns an identifier which shows the dependency between this Line and the RealPos pos. Possible return values are: \n
    -1 -- this straight line don't touch the Position pos \n
    1 -- this straight line touch the Position pos and pos lies before p0_ \n
    2 -- this straight line touch the Position pos and pos lies at the same position like pos p0_\n
    3 -- this straight line touch the Position pos and pos lies within the definition positions p0_ and p1_\n
    4 -- this straight line touch the Position pos and pos lies at the same position like pos p1_\n
    5 -- this straight line touch the Position pos and pos lies behind p1_\n
  */
  int touch( const RealPos & pos, double tol = TOLERANCE, bool verbose = false ) const ;
  /*! */
  bool compare( const Line & line, double tol = TOLERANCE );
  /*! */
  bool checkValidity();
  /*! The straight line is not valid if p0_ == p1_ or it is not initialized by default constructor.*/
  bool valid() const { return valid_; }
  /*! */
  void setValid( bool valid ) { valid_ = valid; }

  /*! Returns the distance betweem p0 and p1.*/
  double length( ) const { return p0_.distance( p1_ ); }

protected:
  RealPos p0_;
  RealPos p1_;
  bool valid_;
};

ostream & operator << ( ostream & str, const Line & l );
} // namespace MyMesh;

#endif // LINE__H

/*
$Log: line.h,v $
Revision 1.4  2009/02/02 10:43:04  carsten
*** empty log message ***

Revision 1.3  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.2  2005/01/06 17:07:42  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
