// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "numfunct.h"

int sign( double a ){
  if ( a == 0.0 ) return 0;
  return (int)rint(fabs(a)/a);
}

double round( double val, unsigned int count) { 
  if ( count < 11 ){
    double v[] = { 1, 10, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9, 1e10, 1e11, 1e12 };
    return floor( val * v[ count ] + 0.5 ) / v[ count ]; 
  } else{
    cerr << WHERE_AM_I << " precision " << count << " to high." << endl;
    return -1;
  }
}

MyVec::STLMatrix TriangleLinearA(  ){
  MyVec::STLMatrix mat( 3, 3);
  mat[ 0 ][ 0 ] =  1.0; mat[ 0 ][ 1 ] = 0.0; mat[ 0 ][ 2 ] = 0.0;
  mat[ 1 ][ 0 ] = -1.0; mat[ 1 ][ 1 ] = 1.0; mat[ 1 ][ 2 ] = 0.0;
  mat[ 2 ][ 0 ] = -1.0; mat[ 2 ][ 1 ] = 0.0; mat[ 2 ][ 2 ] = 1.0;

  return mat;
}
MyVec::STLMatrix TriangleQuadA(  ){
  double matMat[6][6] = {{1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
			   { -3.0, -1.0, 0.0, 4.0, 0.0, 0.0 },
			   { -3.0, 0.0, -1.0, 0.0, 0.0, 4.0 },
			   {  2.0, 2.0, 0.0, -4.0, 0.0, 0.0 },
			   {  4.0, 0.0, 0.0, -4.0, 4.0, -4.0 },
			   {  2.0, 0.0, 2.0, 0.0, 0.0, -4.0 }};
  MyVec::STLMatrix mat( 6, 6 );
  for ( int i = 0; i < 6; i ++ ){
    for ( int j = 0; j < 6; j ++ ){
      mat[i][j] = matMat[i][j];
    }
  }
  return mat;
}
MyVec::STLMatrix TetrahedronLinearA( ){
  MyVec::STLMatrix mat( 4, 4 );
  mat[ 0 ][ 0 ] = 1;
  mat[ 1 ][ 1 ] = 1;
  mat[ 2 ][ 2 ] = 1;
  mat[ 3 ][ 3 ] = 1;
  mat[ 1 ][ 0 ] = -1;
  mat[ 2 ][ 0 ] = -1;
  mat[ 3 ][ 0 ] = -1;
  return mat;
}
MyVec::STLMatrix TetrahedronQuadA( ){
  double matMat[ 10 ][ 10 ] =    {{ 1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.},
			       { -3., -1.,  0.,  0.,  4.,  0.,  0.,  0.,  0.,  0.},
			       { -3.,  0., -1.,  0.,  0.,  4.,  0.,  0.,  0.,  0.},
			       { -3.,  0.,  0., -1.,  0.,  0.,  4.,  0.,  0.,  0.},
			       {  2.,  2.,  0.,  0., -4.,  0.,  0.,  0.,  0.,  0.},
			       {  2.,  0.,  2.,  0.,  0., -4.,  0.,  0.,  0.,  0.},
			       {  2.,  0.,  0.,  2.,  0.,  0., -4.,  0.,  0.,  0.},
			       {  4.,  0.,  0.,  0., -4., -4.,  0.,  4.,  0.,  0.},
			       {  4.,  0.,  0.,  0.,  0., -4., -4.,  0.,  4.,  0.},
			       {  4.,  0.,  0.,  0., -4.,  0., -4.,  0.,  0.,  4.}};

  MyVec::STLMatrix mat( 10, 10 );
  for ( int i = 0; i < 10; i ++ ){
    for ( int j = 0; j < 10; j ++ ){
      mat[i][j] = matMat[i][j];
    }
  }
  return mat;
}

vector < IntegrateTerm > intLinTriangle_u_xi2(){ // u_xi^2
  return parseString( "a2a2" );
}
vector < IntegrateTerm > intLinTriangle_u_eta2(){ // u_eta^2
  return parseString( "a3a3" );
}
vector < IntegrateTerm > intLinTriangle_u_xi_u_eta(){ // u_xi u_eta
  return parseString( "a2a3" );
}
vector < IntegrateTerm > intLinTriangle_u2(){ // u * u
  return parseString( "a1a1 + 2a1a3eta + a3a3etaeta + 2a1a2xi"
		      " + 2a2a3etaxi + a2a2xixi" );
}

vector < IntegrateTerm > intQuadTriangle_u_xi2(){// u_xi^2
  return parseString( "a2a2 + 2a2a5eta + a5a5etaeta + 4a2a4xi + "
		      "4a4a5etaxi + 4a4a4xixi");
}
vector < IntegrateTerm > intQuadTriangle_u_eta2(){  // u_eta^2
  return parseString( "a3a3 + 4a3a6eta + 4a6a6etaeta + 2a3a5xi"
		      "+ 4a5a6etaxi + a5a5xixi");
}
vector < IntegrateTerm > intQuadTriangle_u_xi_u_eta(){ // u_xi_u_eta
  return parseString( "a2a3 + a3a5eta + 2a2a6eta + 2a5a6etaeta"
		      "+2a3a4xi + a2a5xi + a5a5etaxi + 4a4a6etaxi"
		      "+2a4a5xixi");
}
vector < IntegrateTerm > intQuadTriangle_u2(){  // u * u
  return parseString( "a1a1 + 2a1a3eta + a3a3etaeta + 2a1a6etaeta + "
		      "2a3a6etaetaeta + a6a6etaetaetaeta + 2a1a2xi + 2a2a3etaxi + "
		      "2a1a5etaxi + 2a3a5etaetaxi + 2a2a6etaetaxi + "
		      "2a5a6etaetaetaxi + a2a2xixi + 2a1a4xixi + "
		      "2a3a4etaxixi + 2a2a5etaxixi + a5a5etaetaxixi + "
		      "2a4a6etaetaxixi + 2a2a4xixixi + 2a4a5etaxixixi + " 
		      "a4a4xixixixi");
}

vector < IntegrateTerm > intLinTetrahedron_u_xi2(){ // u_xi^2
  return parseString( "a2a2", 3 );
}
vector < IntegrateTerm > intLinTetrahedron_u_eta2(){ // u_eta^2
  return parseString( "a3a3", 3 );
}
vector < IntegrateTerm > intLinTetrahedron_u_zeta2(){ // u_zeta^2
  return parseString( "a4a4", 3 );
}
vector < IntegrateTerm > intLinTetrahedron_u_xi_u_eta(){ // u_xi u_eta
  return parseString( "a2a3", 3 );
}
vector < IntegrateTerm > intLinTetrahedron_u_xi_u_zeta(){ // u_xi u_eta
  return parseString( "a2a4", 3 );
}
vector < IntegrateTerm > intLinTetrahedron_u_eta_u_zeta(){ // u_xi u_eta
  return parseString( "a3a4", 3 );
}
vector < IntegrateTerm > intLinTetrahedron_u2(){ // u_xi u_eta
  return parseString( "a1a1 + 2a1a4chi + a4a4chichi + 2a1a3eta + "
		      "2a3a4chieta + a3a3etaeta + 2a1a2xi + "
		      "2a2a4chixi + 2a2a3etaxi + a2a2xixi", 3 );
}

vector < IntegrateTerm > intQuadTetrahedron_u_xi2(){ // u_xi^2
  return parseString("a2a2 + 2a10a2chi + a10a10chichi + 2a2a8eta + "
		     "2a10a8chieta + a8a8etaeta + 4a2a5xi + "
		     "4a10a5chixi + 4a5a8etaxi + 4a5a5xixi",3);
}
vector < IntegrateTerm > intQuadTetrahedron_u_eta2(){
  return parseString("a3a3 + 2a3a9chi + a9a9chichi + 4a3a6eta +"
		     "4a6a9chieta + 4a6a6etaeta + 2a3a8xi +"
		     "2a8a9chixi + 4a6a8etaxi + a8a8xixi",3);
}
vector < IntegrateTerm > intQuadTetrahedron_u_zeta2(){
  return parseString("a4a4 + 4a4a7chi + 4a7a7chichi + 2a4a9eta + "
		     "4a7a9chieta + a9a9etaeta + 2a10a4xi + "
		     "4a10a7chixi + 2a10a9etaxi + a10a10xixi",3);
}
vector < IntegrateTerm > intQuadTetrahedron_u_xi_u_eta(){
  return parseString("a2a3 + a10a3chi + a2a9chi + a10a9chichi + 2a2a6eta + "
		     "a3a8eta + 2a10a6chieta + a8a9chieta + "
		     "2a6a8etaeta + 2a3a5xi + a2a8xi + a10a8chixi + "
		     "2a5a9chixi + 4a5a6etaxi + a8a8etaxi + "
		     "2a5a8xixi",3);
}
vector < IntegrateTerm > intQuadTetrahedron_u_xi_u_zeta(){
  return parseString("a2a4 + a10a4chi + 2a2a7chi + 2a10a7chichi + "
		     "a4a8eta + a2a9eta + 2a7a8chieta + a10a9chieta + "
		     "a8a9etaeta + a10a2xi + 2a4a5xi + a10a10chixi + "
		     "4a5a7chixi + a10a8etaxi + 2a5a9etaxi + "
		     "2a10a5xixi",3);
}
vector < IntegrateTerm > intQuadTetrahedron_u_eta_u_zeta(){
  return parseString("a3a4 + 2a3a7chi + a4a9chi + 2a7a9chichi + "
		     "2a4a6eta + a3a9eta + 4a6a7chieta + a9a9chieta + "
		     "2a6a9etaeta + a10a3xi + a4a8xi + 2a7a8chixi + "
		     "a10a9chixi + 2a10a6etaxi + a8a9etaxi + "
		     "a10a8xixi",3);
}
vector < IntegrateTerm > intQuadTetrahedron_u2(){
  return parseString("a1a1 + 2a1a4chi + a4a4chichi + 2a1a7chichi + "
		     "2a4a7chichichi + a7a7chichichichi + 2a1a3eta + "
		     "2a3a4chieta + 2a1a9chieta + 2a3a7chichieta + "
		     "2a4a9chichieta + 2a7a9chichichieta + a3a3etaeta + "
		     "2a1a6etaeta + 2a4a6chietaeta + 2a3a9chietaeta + "
		     "2a6a7chichietaeta + a9a9chichietaeta + 2a3a6etaetaeta + "
		     "2a6a9chietaetaeta + a6a6etaetaetaeta + 2a1a2xi + "
		     "2a1a10chixi + 2a2a4chixi + 2a10a4chichixi + "
		     "2a2a7chichixi + 2a10a7chichichixi + 2a2a3etaxi + "
		     "2a1a8etaxi + 2a10a3chietaxi + 2a4a8chietaxi + "
		     "2a2a9chietaxi + 2a7a8chichietaxi + "
		     "2a10a9chichietaxi + 2a2a6etaetaxi + "
		     "2a3a8etaetaxi + 2a10a6chietaetaxi + "
		     "2a8a9chietaetaxi + 2a6a8etaetaetaxi + a2a2xixi + "
		     "2a1a5xixi + 2a10a2chixixi + 2a4a5chixixi + "
		     "a10a10chichixixi + 2a5a7chichixixi + "
		     "2a3a5etaxixi + 2a2a8etaxixi + "
		     "2a10a8chietaxixi + 2a5a9chietaxixi + "
		     "2a5a6etaetaxixi + a8a8etaetaxixi + 2a2a5xixixi + "
		     "2a10a5chixixixi + 2a5a8etaxixixi + a5a5xixixixi",3);
}

double fakultaet( int max ){
  double result = 1.0;
  if ( max == 0 ) return 1.0;
  for ( int i = 1; i <= max; i ++ ) result *= (double)i;
  return result;
} 

MyVec::STLMatrix createMatrix( size_t dim, vector < IntegrateTerm > terms ){
  MyVec::STLMatrix result( dim, dim );

  for ( size_t k = 0, kmax = terms.size(); k < kmax; k ++ ){
    int i = terms[ k ].i() - 1;
    int j = terms[ k ].j() - 1;
    double val = terms[ k ].val();

    result[ i ][ j ] += val / 2.0;
    result[ j ][ i ] += val / 2.0;
  }
  return result;
}

IntegrateTerm analyseTerm( const string & termstring, int dim ){
  double faktor = 0;
  int pos = 0, i = 0, j = 0, x = 0, y = 0, z = 0;

  faktor = (double)atoi( termstring.c_str() );
  if ( faktor == 0 ) faktor = 1.0;

  pos = -1;
  pos = termstring.find( "a", pos + 1 );
  if ( pos != -1 ){
    i = atoi( termstring.substr( pos + 1 ).c_str() );
  } else { 
    cerr << "i not found " << endl; 
  }
  pos = termstring.find( "a", pos + 1 );
  if ( pos != -1 ){
    j = atoi( termstring.substr( pos + 1 ).c_str() );
  } else { 
    cerr << "j not found " << endl; 
  }

  pos = -1;
  while ( (int)(pos = termstring.find( "xi", pos + 1 ) ) != -1 ) x ++;
  pos = -1;
  while ( (int)(pos = termstring.find( "eta", pos + 1 ) ) != -1 ) y ++;
  pos = -1;
  while ( (int)(pos = termstring.find( "chi", pos + 1 ) ) != -1 ) z ++;

  if ( i == 0 || j == 0 ) cerr << "can not determine i or j " << i << "\t" << j << endl;

  IntegrateTerm term( faktor, i, j, x, y, z, dim );
  return term;
};

vector < IntegrateTerm > parseString( const string & mathematicaOut, int dim ){
  vector < IntegrateTerm > terms;

  int posStart = 0, posEnd = -1;

  while ( (int)(posEnd = mathematicaOut.find( "+", posStart )) != -1 ){
  terms.push_back( analyseTerm(mathematicaOut.substr( posStart, posEnd - posStart ), dim ) );
    posStart = posEnd + 1 ;
  }
  terms.push_back( analyseTerm( mathematicaOut.substr( posStart, posEnd - posStart ), dim ) );
  return terms;
}

real_ BesselI0(real_ x){
  real_ t=x/3.75, res=0, epsilon=1.6E-7;
  
  if ( (x > -3.75) && (x < 3.75) ) {
    res = 1 + 3.5156229 * t*t + 
      3.0899424 * t*t*t*t +
      1.2067492 * t*t*t*t*t*t +
      0.2659732 * t*t*t*t*t*t*t*t +
      0.360768  * t*t*t*t*t*t*t*t*t*t +
      0.0045813 * t*t*t*t*t*t*t*t*t*t*t*t + epsilon;
  }else cerr << __FILE__ << __LINE__ << "\nBesselFunktion I_0 f�r x = " << x
	     <<" noch nicht implementiert. (Siehe Abramowitz: Handbook of math. functions)" << endl;
  return(res);
}

real_ BesselI1(real_ x){
  real_ t=x/3.75, res=0, epsilon=8E-9;
  
  if ((x > -3.75) && (x < 3.75)) {
    res = 0.5 + 0.87890594 * t*t + 
      0.51498869 * t*t*t*t +
      0.15084934 * t*t*t*t*t*t +
      0.02658733 * t*t*t*t*t*t*t*t +
      0.00301532 * t*t*t*t*t*t*t*t*t*t +
      0.00032411 * t*t*t*t*t*t*t*t*t*t*t*t + epsilon;
  }else cerr << __FILE__ << __LINE__ << "\nBesselFunktion I_1 f�r x = " << x
	     <<" noch nicht implementiert. (Siehe Abramowitz: Handbook of math. functions)" << endl;
  return(res * x);
}

double besselI0( double x ){
   double ax,ans;
   double y;

   if ((ax=fabs(x)) < 3.75) {
      y=x/3.75,y=y*y;
      ans=1.0+y*(3.5156229+y*(3.0899424+y*(1.2067492
         +y*(0.2659732+y*(0.360768e-1+y*0.45813e-2)))));
   } else {
      y=3.75/ax;
      ans=(exp(ax)/sqrt(ax))*(0.39894228+y*(0.1328592e-1
         +y*(0.225319e-2+y*(-0.157565e-2+y*(0.916281e-2
         +y*(-0.2057706e-1+y*(0.2635537e-1+y*(-0.1647633e-1
         +y*0.392377e-2))))))));
   }
   return ans;
}

double besselI1( double x){
   double ax,ans;
   double y;

   if ((ax=fabs(x)) < 3.75) {
      y=x/3.75,y=y*y;
      ans=ax*(0.5+y*(0.87890594+y*(0.51498869+y*(0.15084934
         +y*(0.2658733e-1+y*(0.301532e-2+y*0.32411e-3))))));
   } else {
      y=3.75/ax;
      ans=0.2282967e-1+y*(-0.2895312e-1+y*(0.1787654e-1
         -y*0.420059e-2));
      ans=0.39894228+y*(-0.3988024e-1+y*(-0.362018e-2
         +y*(0.163801e-2+y*(-0.1031555e-1+y*ans))));
      ans *= (exp(ax)/sqrt(ax));
   }
   return x < 0.0 ? -ans : ans;
}

double besselK( uint i, double x ){
    //** i == 0 besselK0( double x )
    //** i == 1 besselK1( double x )
    return 0.0; 
}
    
double besselK0( double x ){
   double y = 0.0, res = 0.0;

   if (x <= 2.0) {
      y = x * x / 4.0;
      res = ( -log( x / 2.0 ) * besselI0( x ) ) + 
	( -0.57721566 
	  + y * ( 0.42278420 
		  + y * ( 0.23069756 
			  + y * ( 0.3488590e-1 
				 + y * ( 0.262698e-2
					+ y * ( 0.10750e-3 
						+ y * 0.74e-5 ) ) ) ) ) );
   } else {
        y = 2.0 / x;
        res = ( exp( -x ) / sqrt( x ) ) * 
       ( 1.25331414 
	 + y * ( -0.7832358e-1
		 + y * ( 0.2189568e-1
			 + y * ( -0.1062446e-1 
				 + y * ( 0.587872e-2
					 + y * ( -0.251540e-2
						+ y * 0.53208e-3 ) ) ) ) ) );
   }
   return res;
}
double besselK1( double x ){
   double y = 0.0, ans = 0.0;

   if (x <= 2.0) {
      y=x*x/4.0;
      ans=(log(x/2.0)*besselI1(x))+(1.0/x)*(1.0+y*(0.15443144
         +y*(-0.67278579+y*(-0.18156897+y*(-0.1919402e-1
         +y*(-0.110404e-2+y*(-0.4686e-4)))))));
   } else {
      y=2.0/x;
      ans=(exp(-x)/sqrt(x))*(1.25331414+y*(0.23498619
         +y*(-0.3655620e-1+y*(0.1504268e-1+y*(-0.780353e-2
         +y*(0.325614e-2+y*(-0.68245e-3)))))));
   }
   return ans;
}

real_ BesselK0(real_ x){
  real_ res=0;
  
  if (x<=2.){
    res = - log(x/2) * BesselI0(x) - 0.57721566 +
      0.42278420 * (x*x)/4 +
      0.23069756 * (x*x*x*x)/16 +
      0.03488590 * (x*x*x*x*x*x)/64 +
      0.00262698 * (x*x*x*x*x*x*x*x)/256 +
      0.00010750 * (x*x*x*x*x*x*x*x*x*x)/1024 +
      0.00000740 * (x*x*x*x*x*x*x*x*x*x*x*x)/4096 + 1e-8;
  }else{
    res = 1.25331414 - 0.07832358 * (2.0 / x )
      + 0.02189568 * 4.0 / (x*x)          
      - 0.01062446 * 8.0 / (x*x*x)        
      + 0.00587872 * 16.0 / (x*x*x*x)     
      - 0.00251540 * 32.0 / (x*x*x*x*x)   
      + 0.00053208 * 64.0 / (x*x*x*x*x*x) + 1.9e-7;
    res = res / ( sqrt(x) * exp(x) );
  }
  return(res);
}

real_ BesselK1(real_ x){
  real_ res=0, epsilon=2.2E-7;
  
  if (x<=2.){
   res = x * log(x/2) * BesselI1(x) + 1 + 
     0.15443144 * (x*x)/4 -
     0.67278579 * (x*x*x*x)/16 -
     0.18156897 * (x*x*x*x*x*x)/64 -
     0.01919402 * (x*x*x*x*x*x*x*x)/256 -
     0.00110404 * (x*x*x*x*x*x*x*x*x*x)/1024 -
     0.00004686 * (x*x*x*x*x*x*x*x*x*x*x*x)/4096 + 8E-9;
   res/=x;
  }else{
    res = 1.25331414 + 0.23498619 * (2/x) 
      - 0.03655620 * 4/(x*x)          
      + 0.01504268 * 8/(x*x*x)        
      - 0.00780353 * 16/(x*x*x*x)     
      + 0.00325614 * 32/(x*x*x*x*x)   
      - 0.00068245 * 64/(x*x*x*x*x*x) + epsilon;
    res/=(sqrt(x)*exp(x));
  }
  return(res);
}

real_ Bessel(real_ x){
  return(BesselK1(x)/BesselK0(x));
}

void GaussLaguerre( size_t n, vector < double > & x, vector < double > & w ){
//  taken from matlab-code (Thomas Guenther)
// function [x, w] = gaulag(n)
//  GAULAG - Gauss-Laguerre Integration Points
//  [x,w] = gaulag(n)
//  Given alf = 0.0, the parameter alpha of the Laguerre polynomials, this routine
//  returns arrays x[1..n] and w[1..n] containing the abscissas and weights
//  of the n-point Gauss-Laguerre quadrature formula. The smallest abscissa
//  is returned in x[1], the largest in x[n].
//  For a description of the following routines see
//  Numerical Recipes, Press et a

  if ( x.size() != n ) x.resize( n );
  if ( w.size() != n ) w.resize( n );
  
  double epsilon = 3.0e-11;
  int maxiter = 10;
  
  double z = 0.0, z1 = 0.0, p1 = 1.0, p2 = 0.0, p3 = 0.0, pp = 0.0;
  int ai = 0;

  for ( size_t i = 1; i <=n; i ++ ){ //	Loop over desired roots
    if ( i == 1 ){
      z = 3.0 / ( 1.0 + 2.4 * n );
    } else if ( i == 2 ){
      z = z + 15.0 / ( 1.0 +2.5 * n );
    } else { 
      ai = i - 2;
      z = z + ( 1.0 + 2.55 * ai ) / ( 1.9 * ai ) *( z - x[ ai - 1 ] );
    }

    for ( int its = 1; its <= maxiter; its ++ ){
      p1 = 1.0;
      p2 = 0.0;
   
      for ( size_t j = 1; j <= n; j ++ ){
	p3 = p2;
	p2 = p1;
	p1 = ( ( 2.0 * j - 1 - z ) * p2 - ( j - 1 ) * p3 ) / j;
      }
      pp = n *( p1 - p2 ) / z;
      z1 = z;
      z  = z1 - p1 / pp;

      if( fabs( z - z1 ) <= epsilon) break;
    }
    x[ i - 1 ] = z;
    w[ i - 1 ] = -1.0 / ( pp * n * p2 );
  }
}

void GaussLegendre( double x1, double x2, size_t n, vector < double > & x, vector < double > & w ){
//  taken from matlab-code (Thomas Guenther)
//  function [x, w] = gauleg(x1, x2, n)
//  Given the lower and upper limits of integration x1 and x2 and given n,
//  this routine returns arrays x(1..n) and w(1..n) of length n,
//  containing the abscissas and weights of the Gauss-Legendre
//  n-point quadrature formula.
//  For a description of the following routines see
//  Numerical Recipes, Press et al.

  if ( x.size() != n ) x.resize( n );
  if ( w.size() != n ) w.resize( n );

  double epsilon = 3.0e-6;
  
  double m = ( n + 1.0 ) / 2.0 ;
  double xm = 0.5 * ( x2 + x1 );
  double xl = 0.5 * ( x2 - x1 );
  
  double z = 0.0, z1 = 0.0, p1 = 0.0, p2 = 0.0, p3 = 0.0, pp = 0.0; 

  for ( int i = 1; i <= m; i ++ ){
   z = cos ( PI_ * ( i - 0.25 ) / ( n + 0.5 ) );   

   // Starting with the above approximation to the ith root, we enter
   // the main loop of refinements by Newton's method
   z1 = z + 2.0 * epsilon;

   while ( fabs( z - z1 ) > epsilon ){
     p1 = 1.0; 
     p2 = 0.0;
     for ( size_t j = 1; j <= n; j ++ ){
       p3 = p2; 
       p2 = p1;
       p1 = ( ( 2.0 * j - 1.0 ) * z * p2 - ( j - 1.0 ) * p3 ) / (double)j;
     }

     // p1 is now the desired Legendre polynomial. We next compute pp,
     // its derivative, by a standard relation involving also p2, the
     // polynomial of one lower order
     pp = (double)n * ( z * p1 - p2 ) / ( z * z - 1.0 );
     z1 = z;
     z = z1 - p1 / pp; // Newtons method
   }
   // Scale the root to the desired interval, and put in its
   // symmetric counterpart
   x[ i - 1 ] = xm - xl * z;
   x[ n - i ] = xm + xl * z;
   //   x[ n +1 - i ] = xm + xl * z;
   //Compute the weight and ist symmetric counterpart
   w[ i - 1 ] = 2.0 * xl / ( ( 1.0 -z * z ) *pp *pp );
   w[ n - i ] = w[ i - 1 ];
   //   w[ n + 1 - i ] = w[ i - 1 ];
  }
}

real_ GaussLegendreQuadraturAbscissas(int n, int k){
  // Gaussian-Legendre Integration from [0,1] \int x^k f(x) dx; k=0;
  // TableValues from Abramowitsch
  // [x,w]=gauleg( 0,1,n ), by thomas guenther
  int max = 16;
  real_ abscissas[ 16 + 1 ][ 16 + 1 ]={{0},
			   /* 1 */{0, 0.5},
			   /* 2 */{0, 0.2113248654, 0.7886751346},
			   /* 3 */{0, 0.1127016654, 0.5, 0.8872983346},
			   /* 4 */{0, 0.0694318442, 0.3300094782, 0.6699905218, 0.9305681558},
			   /* 5 */{0, 0.0469100770, 0.2307653449, 0.5, 0.7692346551, 0.9530899230},
			   /* 6 */{0, 0.0337652429, 0.1693953068, 0.3806904070, 0.6193095930, 
				   0.8306046932, 0.9662347571},
			   /* 7 */{0, 0.0254460438, 0.1292344072, 0.2970774243,
				   0.5, 0.702925757, 0.8707655928, 0.9745539562},
			   /* 8 */{0, 0.0198550718, 0.1016667613, 0.2372337950, 0.4082826788, 
				   0.5917173212, 0.7627662050, 0.8983332387, 0.9801449282},
			   /* 9 */{0, 0.0159,0.0820,0.1933,0.3379,0.5000,0.6621,0.8067,
				   0.9180,0.9841},
			   /*10 */{0, 0.0130,0.0675,0.1603,0.2833,0.4256,0.5744,0.7167,
				   0.8397,0.9325,0.9870},
         	       /*11 */{ 0.0, 0.0109, 0.0565, 0.1349, 0.2405, 0.3652, 0.5000, 0.6348,
					0.7595,    0.8651,    0.9435,    0.9891},
			   /*12 */{0, 0.0092,0.0479,0.1150,0.2063,0.3161,0.4374,0.5626,
				   0.6839,0.7937,0.8850,0.9521,0.9908},
			   /*13 */{0, 0.0079,0.0412,0.0992,0.1788,0.2758,0.3848,0.5000,
				   0.6152,0.7242,0.8212,0.9008,0.9588,0.9921},
			   /*14 */{0, 0.0069, 0.0358, 0.0864, 0.1564, 0.2424, 0.3404, 0.4460,
				   0.5540, 0.6596, 0.7576, 0.8436, 0.9136, 0.9642, 0.9931},
			   /*15 */{0, 0.0060, 0.0314, 0.0759, 0.1378, 0.2145, 0.3029, 0.3994,
				   0.5000, 0.6006, 0.6971, 0.7855, 0.8622, 0.9241, 0.9686,
				   0.9940},
			   /*16 */{0, 0.0053, 0.0277, 0.0672, 0.1223, 0.1911, 0.2710, 0.3592,
				   0.4525, 0.5475, 0.6408, 0.7290, 0.8089, 0.8777, 0.9328,
				   0.9723, 0.9947}};


  if (n > max) return 0;
  if (k > n) cerr << __FILE__ << __LINE__<< "AbscissValue " << k << "/" << n 
		  << " not defined." << endl;
  return abscissas[ n ][ k ];
}

real_ GaussLegendreQuadraturWeight( int n, int k ){
  // Gaussian-Legendre Integration from [0,1] \int x^k f(x) dx; k=0; 
  // TableValues from Abramowitsch
  // [x,w]=gauleg( 0,1,n ), by thomas guenther
  int max = 16;
  real_ weight[ 16 + 1 ][ 16 + 1 ]={{0},
			/* 1 */{0, 1},
			/* 2 */{0, 0.5, 0.5},
			/* 3 */{0, 0.2777777778, 0.4444444444, 0.2777777778},
			/* 4 */{0, 0.1739274226, 0.3260725774, 0.3260725774, 0.1739274226},
			/* 5 */{0, 0.1184634425, 0.2393143352, 0.2844444444, 0.2393143352,
				0.1184634425},
			/* 6 */{0, 0.0856622462, 0.1803807865, 0.2339569673,0.2339569673,
				0.1803807865, 0.0856622462},
			/* 7 */{0, 0.0647424831, 0.1398526957, 0.1909150253, 0.2089795918,
				0.1909150253, 0.1398526957, 0.0647424831},
			/* 8 */{0, 0.0506142681, 0.1111905172, 0.1568533229, 0.1813418917,
				0.1813418917, 0.1568533229, 0.1111905172, 0.0506142681},
			/* 9 */{0, 0.0406,0.0903,0.1303,0.1562,0.1651,0.1562,0.1303,0.0903,
				0.0406},
			/*10 */{0, 0.0333,0.0747,0.1095,0.1346,0.1478,0.1478,0.1346,0.1095,
				0.0747,0.0333},
			/*11 */{0, 0.0278, 0.0628, 0.0931, 0.1166, 0.1314, 0.1365, 0.1314, 0.1166,
				0.0931, 0.0628, 0.0278},
			/*12 */{0, 0.0236, 0.0535, 0.0800, 0.1016,0.1167,0.1246,0.1246,0.1167,
				0.1016,0.0800,0.0535,0.0236},
			/*13 */{0, 0.0202, 0.0461, 0.0694, 0.0891, 0.1039, 0.1131, 0.1163,
				0.1131,0.1039,0.0891,0.0694,0.0461,0.0202},
		      /*14 */{0, 0.0176, 0.0401, 0.0608, 0.0786, 0.0928, 0.1026, 0.1076,
				0.1076, 0.1026, 0.0928, 0.0786, 0.0608, 0.0401, 0.0176},
		      /*15 */{0, 0.0154, 0.0352, 0.0536, 0.0698, 0.0831, 0.0931, 0.0992,
				0.1013, 0.0992, 0.0931, 0.0831, 0.0698, 0.0536, 0.0352,
				0.0154},
		      /*16 */{0, 0.0136, 0.0311, 0.0476, 0.0623, 0.0748, 0.0846, 0.0913,
			      0.0947, 0.0947, 0.0913, 0.0846, 0.0748, 0.0623, 0.0476,
			      0.0311, 0.0136}};
  if ( n > max ) return 0;
  if ( k > n ) cerr << __FILE__ << __LINE__<< "WeightValue " << k << "/" << n 
		  << " not defined." << endl;
  return weight[ n ][ k ];
}

real_ GaussLaguerreQuadraturAbscissas(int n, int k){
  // Gaussian-Laguerre-Typ Quadration from [0,infty] \int e^{-x} f(x) dx
  // TableValues from Bronstein
  // [x,w]=gaulag( n ), by thomas guenther
  int max = 10;
  real_ abscissas[ 10 + 1 ][ 10 + 1 ]={{0},
			   /* 1 */{0, 0.5},
			   /* 2 */{0, 0.585786437627, 3.414213562373},
			   /* 3 */{0, 0.415774556783, 2.294280360279, 6.289945082937},
			   /* 4 */{0, 0.32254769, 1.74576110, 4.53662030, 9.35907091},
			   /* 5 */{0, 0.26356032, 1.41340306, 3.59642577, 7.08581001, 
				   12.64080084},
			   /* 6 */{0, 0.22284660, 1.18893210, 2.99273633, 5.77514357, 
				   9.83746742, 15.98287398},
			   /* 7 */{0, 0.1930, 1.0267, 2.5679, 4.9004, 8.1822, 12.7342, 19.3957 },
			   /* 8 */{0, 0.1703, 0.9037, 2.2511, 4.2667, 7.0459, 10.7585, 15.7407, 
				   22.8631},
			   /* 9 */{0, 0.15230, 0.8072, 2.0051, 3.7835, 6.2050, 9.3730, 13.4662, 
				   18.8336,  26.3741},
			   /* 10*/{0, 0.1378, 0.7295, 1.8083, 3.4014, 5.5525, 8.3302, 11.8438, 
				   16.2793, 21.9966, 29.9207}};
			
  if (n > max) return 0;
  if (k > n) cerr << __FILE__ << __LINE__<< "AbscissValue " << k << "/" << n 
		  << " not defined." << endl;
  return( abscissas[n][k] );
}

real_ GaussLaguerreQuadraturWeight(int n, int k){
  // Gaussian-Laguerre-Typ Quadration from [0,infty] \int e^{-x} f(x) dx; 
  // TableValues from Bronstein
  // [x,w]=gaulag( n ), by thomas guenther
  int max = 10;
  real_ weight[ 10 + 1 ][ 10 + 1 ]={{0},
		      /* 1 */{0, 1},
		      /* 2 */{0, 8.53553390593, 1.46446609407},
		      /* 3 */{0, 7.11093009929, 2.78517733569, 1.03892565016},
		      /* 4 */{0, 0.60315410, 0.35741869, 0.03888791, 0.00053929471},
		      /* 5 */{0, 0.52175561, 0.39866681, 0.07594245, 0.36117587E-2, 0.23369972E-4},
		      /* 6 */{0, 0.45896467, 0.41700083, 0.11337338, 0.010399197, 0.2610172E-3,
			      0.89854791E-6},
		      /* 7 */{0, 0.40932,0.42183,0.14713,0.020634,0.001074,1.5865e-05,3.1703e-08},
		      /* 8 */{0, 0.36919,0.41879,0.17579,0.033343,0.0027945,9.0765e-05,8.4857e-07,
			      1.048e-09},
		      /* 9 */{0, 33613,0.41121,0.19929,0.047461,0.0055996,0.00030525,6.5921e-06,
			      4.1108e-08,3.2909e-11},
		      /* 10 */{0, 0.33613,0.41121,0.19929,0.047461,0.0055996,0.00030525,
			       6.5921e-06,4.1108e-08,3.2909e-11}};

  if ( n > max ) return 0.0;
  if ( k > n ) cerr << __FILE__ << __LINE__<< "WeightValue " << k << "/" << n 
		    << " not defined." << endl;
  return weight[ n ][ k ];
}

real_ determinant2x2( real_ a, real_ b, real_ c, real_ d){
  return a * d - b * c;
}

real_ det2( real_ a, real_ b, real_ c, real_ d){
  return a * d - b * c;
}

MyVec::STLMatrix inv( const MyVec::STLMatrix & A ){
  //** das geht viel schoener, aber nicht mehr heute.
  MyVec::STLMatrix Ai( A );
  switch ( A.dim1() ){
  case 2: {
    Ai[ 0 ][ 0 ] = A[ 1 ][ 1 ]; 
    Ai[ 0 ][ 1 ] = -A[ 0 ][ 1 ];
    Ai[ 1 ][ 0 ] = -A[ 1 ][ 0 ];
    Ai[ 1 ][ 1 ] = A[ 0 ][ 0 ];
    Ai /= det( A );
  } break;
  case 3: 
    Ai[ 0 ][ 0 ] =   det2( A[ 1 ][ 1 ], A[ 1 ][ 2 ], A[ 2 ][ 1 ], A[ 2 ][ 2 ] );
    Ai[ 1 ][ 0 ] = - det2( A[ 1 ][ 0 ], A[ 1 ][ 2 ], A[ 2 ][ 0 ], A[ 2 ][ 2 ] );
    Ai[ 2 ][ 0 ] =   det2( A[ 1 ][ 0 ], A[ 1 ][ 1 ], A[ 2 ][ 0 ], A[ 2 ][ 1 ] );

    Ai[ 0 ][ 1 ] = - det2( A[ 0 ][ 1 ], A[ 0 ][ 2 ], A[ 2 ][ 1 ], A[ 2 ][ 2 ] );
    Ai[ 1 ][ 1 ] =   det2( A[ 0 ][ 0 ], A[ 0 ][ 2 ], A[ 2 ][ 0 ], A[ 2 ][ 2 ] );
    Ai[ 2 ][ 1 ] = - det2( A[ 0 ][ 0 ], A[ 0 ][ 1 ], A[ 2 ][ 0 ], A[ 2 ][ 1 ] );

    Ai[ 0 ][ 2 ] =   det2( A[ 0 ][ 1 ], A[ 0 ][ 2 ], A[ 1 ][ 1 ], A[ 1 ][ 2 ] );
    Ai[ 1 ][ 2 ] = - det2( A[ 0 ][ 0 ], A[ 0 ][ 2 ], A[ 1 ][ 0 ], A[ 1 ][ 2 ] );
    Ai[ 2 ][ 2 ] =   det2( A[ 0 ][ 0 ], A[ 0 ][ 1 ], A[ 1 ][ 0 ], A[ 1 ][ 1 ] );

    Ai /= det( A );

    break;
  default:
    cerr << WHERE_AM_I << " matrix inversion of dim not yet implemented -- dim: " << A.dim1() << endl; 
    break;
  }

  return Ai;
}

double det( const MyVec::STLMatrix & A ){
  //** das geht viel schoener, aber nicht mehr heute.
  double det = 0.0;
  switch ( A.dim1() ){
  case 2: det = A[ 0 ][ 0 ] * A[ 1 ][ 1 ] - A[ 0 ][ 1 ] * A[ 1 ][ 0 ]; 
    break;
  case 3: 
    det = A[ 0 ][ 0 ] * ( A[ 1 ][ 1 ] * A[ 2 ][ 2 ] - A[ 1 ][ 2 ] * A[ 2 ][ 1 ] ) -
          A[ 0 ][ 1 ] * ( A[ 1 ][ 0 ] * A[ 2 ][ 2 ] - A[ 1 ][ 2 ] * A[ 2 ][ 0 ] ) +
          A[ 0 ][ 2 ] * ( A[ 1 ][ 0 ] * A[ 2 ][ 1 ] - A[ 1 ][ 1 ] * A[ 2 ][ 0 ] ); 
    break; 
  default:
    cerr << WHERE_AM_I << " matrix determinant of dim not yet implemented -- dim: " << A.dim1() << endl; 
    break;
  }
  return det;
}

real_ determinant( real_ * A[], int dim  ){
  //** das geht viel schoener, aber nicht mehr heute.
  double det = 0.0;
  switch (dim){
  case -1: cerr << "real_ determinante( real_ * A[], int dim  ) dim not defined. " << endl; return 0;
  case 1: det = A[ 0 ][ 0 ]; break;
  case 2: det = A[ 0 ][ 0 ] * A[ 1 ][ 1 ] - A[ 0 ][ 1 ] * A[ 1 ][ 0 ]; break;
  case 3: 
    det = A[ 0 ][ 0 ] * ( A[ 1 ][ 1 ] * A[ 2 ][ 2 ] - A[ 1 ][ 2 ] * A[ 2 ][ 1 ] ) -
          A[ 0 ][ 1 ] * ( A[ 1 ][ 0 ] * A[ 2 ][ 2 ] - A[ 1 ][ 2 ] * A[ 2 ][ 0 ] ) +
          A[ 0 ][ 2 ] * ( A[ 1 ][ 0 ] * A[ 2 ][ 1 ] - A[ 1 ][ 1 ] * A[ 2 ][ 0 ] ); break; 
  case 4:
    break;
  }
  return det;
}

double degToRad( double deg ){
  return deg * PI_ / 180.0;
}

double radToDeg( double rad ){
  return rad * 180.0 / PI_;
}

/*
$Log: numfunct.cpp,v $
Revision 1.7  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.6  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.5  2006/07/11 08:37:25  thomas
error in gaulag corrected

Revision 1.4  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.3  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.2  2005/09/28 13:07:23  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
