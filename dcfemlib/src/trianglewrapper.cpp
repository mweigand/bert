// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "trianglewrapper.h"

#ifdef __cplusplus
extern "C"{
#endif

#ifdef HAVE_TRIANGLE

#define REAL double
#define VOID void
#define TRILIBRARY
#define ANSI_DECLARATORS

#include <triangle.h>

#endif

#ifdef __cplusplus
}
#endif

TriangleWrapper::TriangleWrapper( const Domain2D & domain ) { 
  init_();
}

TriangleWrapper::TriangleWrapper( const Domain2D & domain, Mesh2D & mesh, const string & sw ) { 
  init_();
  switches_ = sw;
  setDomain( domain );
  generate( mesh );
}

TriangleWrapper::~TriangleWrapper(){
  freeMemory_();
#ifdef HAVE_TRIANGLE
  delete mesh_input_;
  delete mesh_output_;
  delete mesh_voronoi_output_;
#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
}

void TriangleWrapper::init_( ){
  switches_ = "p";

#ifdef HAVE_TRIANGLE
  mesh_input_ = new struct triangulateio;
  mesh_output_ = new struct triangulateio;
  mesh_voronoi_output_ = new struct triangulateio;
  allocateOutMemory_();
#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
}

void TriangleWrapper::setSwitches( const string & s ){ switches_ = s; }

void TriangleWrapper::setDomain( const Domain2D & domain ){ 
  transformFromDomain_( domain, *mesh_input_ ); 
}

void TriangleWrapper::generate( Mesh2D & mesh ){
#ifdef HAVE_TRIANGLE
  //cout << switches_ << endl;
  triangulate( (char *)switches_.c_str(), mesh_input_, mesh_output_, mesh_voronoi_output_ );
#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
  transformToMesh_( *mesh_output_, mesh );
}

Mesh2D TriangleWrapper::mesh(){
  Mesh2D mesh;
  transformToMesh_( *mesh_output_, mesh );
  return mesh;
}

void TriangleWrapper::allocateOutMemory_(){
#ifdef HAVE_TRIANGLE

  mesh_output_->pointlist = (REAL *) NULL;
  mesh_output_->pointattributelist = (REAL *) NULL;
  mesh_output_->pointmarkerlist = (int *) NULL;

  mesh_output_->trianglelist = (int *) NULL;
  mesh_output_->triangleattributelist = (REAL *) NULL;

  mesh_output_->segmentlist = (int *) NULL;
  mesh_output_->segmentmarkerlist = (int *) NULL;

  mesh_output_->edgelist = (int *) NULL;
  mesh_output_->edgemarkerlist = (int *) NULL;

#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
}

void TriangleWrapper::freeMemory_(){
#ifdef HAVE_TRIANGLE
  if ( mesh_output_->pointlist != (REAL*)NULL ) free( mesh_output_->pointlist );
  if ( mesh_output_->pointattributelist != (REAL*)NULL ) free( mesh_output_->pointattributelist );
  if ( mesh_output_->pointmarkerlist != (int*)NULL  ) free( mesh_output_->pointmarkerlist ) ;

  if ( mesh_output_->trianglelist != (int*)NULL ) free( mesh_output_->trianglelist );
  if ( mesh_output_->triangleattributelist != (REAL*)NULL ) free( mesh_output_->triangleattributelist );

  if ( mesh_output_->segmentlist != (int*)NULL ) free( mesh_output_->segmentlist );
  if ( mesh_output_->segmentmarkerlist != (int*)NULL ) free( mesh_output_->segmentmarkerlist );

  if ( mesh_output_->edgelist != (int*)NULL ) free( mesh_output_->edgelist );
  if ( mesh_output_->edgemarkerlist != (int*)NULL ) free( mesh_output_->edgemarkerlist );

  delete [] mesh_input_->pointlist;
  delete [] mesh_input_->pointmarkerlist;
  delete [] mesh_input_->segmentlist;
  delete [] mesh_input_->segmentmarkerlist;
  delete [] mesh_input_->holelist;
  delete [] mesh_input_->regionlist;
#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
}

void TriangleWrapper::transformToMesh_( const triangulateio & trimesh, Mesh2D & mesh ){
#ifdef HAVE_TRIANGLE
  mesh.clear();
  //**  Nodes;
  int marker = 0;
  for (int i = 0; i < trimesh.numberofpoints; i ++ ){
    if ( trimesh.pointmarkerlist != (int*)NULL ) marker = trimesh.pointmarkerlist[ i ];
    //      cout << j 
    //  	 << "\t" << trimesh.pointlist[ i ] 
    //  	 << "\t" << trimesh.pointlist[ i + 1 ] 
    //  	 << "\t" << trimesh.pointmarkerlist[ j ]
    //  	 << endl;
    mesh.createNode( trimesh.pointlist[ i*2 ], trimesh.pointlist[ i*2 + 1 ], i, marker );
  }

  //**  Edges / Segments
  if ( trimesh.numberofedges == 0 ){
    for ( int i = 0; i < trimesh.numberofsegments; i ++ ){
      if ( trimesh.segmentmarkerlist != (int*)NULL ) marker = trimesh.segmentmarkerlist[ i ];
      mesh.createEdge( mesh.node( trimesh.segmentlist[ i * 2 ] ), mesh.node(trimesh.segmentlist[ i * 2 + 1 ]), i, marker );
    }
  } else {
    for ( int i = 0; i < trimesh.numberofedges; i ++ ){
      if ( trimesh.edgemarkerlist != (int*)NULL ) marker = trimesh.edgemarkerlist[ i ];
      mesh.createEdge( mesh.node( trimesh.edgelist[ i * 2 ] ), mesh.node( trimesh.edgelist[ i * 2 + 1 ] ), i, marker);
    }
  }
  
 //** Triangles;
  int a = 0, b = 0, c = 0; 
  double attribute = 0.0;
  for (int i = 0; i < trimesh.numberoftriangles; i ++){
    a = trimesh.trianglelist[ i * 3 ];
    b = trimesh.trianglelist[ i * 3 + 1 ];
    c = trimesh.trianglelist[ i * 3 + 2 ];
    if ( trimesh.triangleattributelist != (REAL*)NULL ) attribute = trimesh.triangleattributelist[ i ];

    //     mesh.createTriangle( mesh.node( trimesh.trianglelist[ i*3 ] ),
// 			 mesh.node( trimesh.trianglelist[ i*3 + 1 ] ),
// 			 mesh.node( trimesh.trianglelist[ i*3 + 2 ] ),
// 			 i, trimesh.triangleattributelist[ i ] );

    mesh.createTriangle( mesh.node( a ), mesh.node( b ), mesh.node( c ), i, attribute );

  }

  // //** RegionMarker
//   for (int i = 0; i < trimesh.numberofregions * 4; i += 4){
//     mesh.createRegionMarker( trimesh.regionlist[ i ], trimesh.regionlist[ i + 1 ], 
// 			      trimesh.regionlist[ i + 2 ], trimesh.regionlist[ i + 3 ] );
//   }
//   for (int i = 0; i < trimesh.numberofholes * 2; i += 2){
//     mesh.createRegionMarker( trimesh.holelist[ i ], trimesh.holelist[ i + 1], -1, -1 );
//   }
#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
}

void TriangleWrapper::transformFromDomain_( const Domain2D & domain, triangulateio & trimesh ){
#ifdef HAVE_TRIANGLE

  //** node section
  int nVerts = domain.nodeCount();

  trimesh.numberofpoints = nVerts;
  trimesh.numberofpointattributes = 0;  // only one Parameter

  trimesh.pointlist = new double[ 2 * nVerts ];
  trimesh.pointmarkerlist = new int[ nVerts ];

  for ( int i = 0; i < nVerts; i ++ ){
    trimesh.pointlist[ i * 2 ] = domain.node( i ).x();
    trimesh.pointlist[ i * 2 + 1] = domain.node( i ).y();
    trimesh.pointmarkerlist[ i ] = domain.node( i ).marker();
  }

  //** edge section;
  int nEdges = domain.countEdges();
  int nPolygons = domain.polygonCount();
  int nNodesPoly = 0;
  int edgeCounter = 0;

  trimesh.numberofsegments = nEdges;
  trimesh.segmentlist = new int[ 2 * nEdges ];
  trimesh.segmentmarkerlist = new int[ nEdges ];

  for ( int i = 0; i < nPolygons; i ++ ){
    nNodesPoly = domain.polygon( i ).size();
    if ( nNodesPoly > 1 ) {
      if ( domain.polygon( i )[ 0 ]->id() != domain.polygon( i )[ 1 ]->id() ){
	for ( size_t k = 0, kmax = nNodesPoly -1; k < kmax; k++ ){ 
//  	 cout << edgeCounter << "\t" << domain.polygon( i )[ k ]->id() << "\t"
//  	      << domain.polygon( i )[ k + 1 ]->id() << "\t" << domain.polygon( i ).marker() << endl;
	  trimesh.segmentlist[ edgeCounter * 2 ] = domain.polygon( i )[ k ]->id();
	  trimesh.segmentlist[ edgeCounter * 2 + 1 ] = domain.polygon( i )[ k + 1]->id();
	  trimesh.segmentmarkerlist[ edgeCounter ] = domain.polygon( i ).marker();
	  edgeCounter++;
	}
      }
    }
  }

  //** Holes;
  int nHoles = domain.holeCount();
  trimesh.numberofholes = nHoles;
  trimesh.holelist = new double[ 2 * nHoles +1];
  for ( int i = 0; i < nHoles; i ++ ){
    trimesh.holelist[ i * 2 ] = domain.hole( i ).x();
    trimesh.holelist[ i * 2 + 1 ] = domain.hole( i ).y();
  }

  //** Regions;
  int nRegions = domain.regionCount();
  trimesh.numberofregions = nRegions;
  trimesh.regionlist = new double[ 4 * nRegions + 1 ];

  for ( int i = 0; i < nRegions; i ++ ){
    trimesh.regionlist[ i * 4 ] = domain.region( i ).x();
    trimesh.regionlist[ i * 4 + 1 ] = domain.region( i ).y();
    trimesh.regionlist[ i * 4 + 2 ] = domain.region( i ).attribute();
    trimesh.regionlist[ i * 4 + 3 ] = domain.region( i ).dx();
  }

#else
  cerr << WHERE_AM_I << " Triangle not installed" << endl;
#endif
}


// // void TriangleWrapper::saveNodePosition(int i, double x, double y){
// // #ifdef HAVE_TRIANGLE
// //   mesh_input_->pointlist[ i * 2 ] = x;
// //   mesh_input_->pointlist[ (i * 2) + 1] = y;
// //   inMesh_.node( i ).setX( x );
// //   inMesh_.node( i ).setY( y );
// // #else
// //   cerr << "triangle not found. " << endl;
// // #endif
// // }

// int TriangleWrapper::generateMesh(){
// #ifdef HAVE_TRIANGLE
//   transformFromMesh_( *inMesh_, *mesh_input_ ); 
//   allocateOutMemory_();
//   triangulate( (char *)( switches_.c_str() ), mesh_input_, mesh_output_, mesh_voronoi_output_ );
//   transformToMesh_( *mesh_output_, *outMesh_ );
//   freeMemory_();

//   return 1;
// #else
//   cerr << "triangle not found. " << endl;
//   return 0;
// #endif
// }

// void TriangleWrapper::transformFromMesh_( const BaseMesh & mesh, triangulateio & trimesh ){
// #ifdef HAVE_TRIANGLE
//   //** Nodes

//   trimesh.numberofpoints = mesh.nodeCount();
//   trimesh.numberofpointattributes = 0;  // only one Parameter

//   trimesh.pointlist = new double[2 * trimesh.numberofpoints];
//   trimesh.pointmarkerlist = new int[trimesh.numberofpoints];

//   for (int i=0, k=0; i<trimesh.numberofpoints * 2; i+=2, k++){
//     trimesh.pointlist[i] = mesh.node( k ).x();
//     trimesh.pointlist[i+1] = mesh.node( k ).y();
//     trimesh.pointmarkerlist[k] = mesh.node( k ).marker();
//   }

//   //** Edges;
//   trimesh.numberofsegments = mesh.boundaryCount();
//   trimesh.segmentlist = new int[ 2 * trimesh.numberofsegments ];
//   trimesh.segmentmarkerlist = new int[ trimesh.numberofsegments ];
//   for (int i=0, k=0; i < trimesh.numberofsegments * 2; i+=2, k++){
//     trimesh.segmentlist[i] = dynamic_cast<Edge &>(mesh.boundary( k )).nodeA().id();
//     trimesh.segmentlist[i + 1] = dynamic_cast<Edge &>(mesh.boundary( k )).nodeB().id();
//     trimesh.segmentmarkerlist[k] = mesh.boundary( k ).marker();
//   }
//   //** RegionMarker and Holes
//   int regions = 0, holes = 0;
//   for (VectorpRegionMarker::iterator it = mesh.regions().begin(); it != mesh.regions().end(); it++ ){
//     if ( (*it)->dx() == -1 ) holes ++; else regions ++;
//   }  

//   trimesh.numberofregions = regions;
//   trimesh.numberofholes = holes;

//   trimesh.regionlist = new double[ 4 * regions ];
//   trimesh.holelist = new double[ 2 * holes ];

//   int countholes = 0, countregions = 0;
//   for ( VectorpRegionMarker::iterator it = mesh.regions().begin(); it != mesh.regions().end(); it++){
//     if ( (*it)->dx() == -1 ){
//       trimesh.holelist[ countholes ] = (*it)->x();
//       trimesh.holelist[ countholes + 1 ] = (*it)->y();
//       countholes += 2;
//     } else {
//       trimesh.regionlist[ countregions ] = (*it)->x();
//       trimesh.regionlist[ countregions + 1 ] = (*it)->y();
//       trimesh.regionlist[ countregions + 2 ] = (*it)->attribute();
//       trimesh.regionlist[ countregions + 3 ] = (*it)->dx();
//       countregions += 4;
//     }
//   }
// #else
//   cerr << "triangle not found. " << endl;
// #endif
// }


/*
$Log: trianglewrapper.cpp,v $
Revision 1.21  2006/07/24 16:24:14  carsten
*** empty log message ***

Revision 1.20  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.19  2005/10/30 18:53:04  carsten
*** empty log message ***

Revision 1.18  2005/10/20 16:25:22  carsten
#

Revision 1.17  2005/10/20 14:33:49  carsten
*** empty log message ***

Revision 1.16  2005/10/18 19:18:13  carsten
*** empty log message ***

Revision 1.15  2005/10/18 16:29:56  carsten
*** empty log message ***

Revision 1.13  2005/10/18 12:48:11  carsten
*** empty log message ***

Revision 1.12  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.11  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.10  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.9  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.8  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.7  2005/01/17 18:17:54  carsten
*** empty log message ***

Revision 1.6  2005/01/17 13:44:53  carsten
*** empty log message ***

Revision 1.5  2005/01/14 12:51:27  carsten
*** empty log message ***

Revision 1.4  2005/01/11 19:24:10  carsten
*** empty log message ***

Revision 1.3  2005/01/06 19:46:00  carsten
*** empty log message ***

Revision 1.2  2005/01/06 19:00:32  carsten
*** empty log message ***

Revision 1.1  2005/01/06 16:15:56  carsten
*** empty log message ***

*/
