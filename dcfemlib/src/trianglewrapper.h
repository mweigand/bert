// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef TRIANGLEWRAPPER__H
#define TRIANGLEWRAPPER__H

#include "dcfemlib.h"
using namespace std;

#include "mesh2d.h"
#include "numfunct.h"
#include "domain2d.h"

using namespace MyMesh;
#include <vector>
#include <string>

using namespace std;


class Domain2D;
struct triangulateio;
//   REAL *pointlist;                                               /* In / out */
//   REAL *pointattributelist;                                      /* In / out */
//   int *pointmarkerlist;                                          /* In / out */
//   int numberofpoints;                                            /* In / out */
//   int numberofpointattributes;                                   /* In / out */

//   int *trianglelist;                                             /* In / out */
//   REAL *triangleattributelist;                                   /* In / out */
//   REAL *trianglearealist;                                         /* In only */
//   int *neighborlist;                                             /* Out only */
//   int numberoftriangles;                                         /* In / out */
//   int numberofcorners;                                           /* In / out */
//   int numberoftriangleattributes;                                /* In / out */

//   int *segmentlist;                                              /* In / out */
//   int *segmentmarkerlist;                                        /* In / out */
//   int numberofsegments;                                          /* In / out */

//   REAL *holelist;                        /* In / pointer to array copied out */
//   int numberofholes;                                      /* In / copied out */

//   REAL *regionlist;                      /* In / pointer to array copied out */
//   int numberofregions;                                    /* In / copied out */

//   int *edgelist;                                                 /* Out only */
//   int *edgemarkerlist;            /* Not used with Voronoi diagram; out only */
//   REAL *normlist;                /* Used only with Voronoi diagram; out only */
//   int numberofedges;                                             /* Out only */
// };

class DLLEXPORT TriangleWrapper{
public:
  TriangleWrapper( const Domain2D & inMesh );

  /*! Constructor inititialize with debug mode and tabbing */
  //  TriangleWrapper( const Mesh2D & inMesh, BaseMesh & outMesh );
  TriangleWrapper( const Domain2D & inMesh, Mesh2D & outMesh, const string & sw );

  /*! Destructs and free memory for \ref BaseMesh _inMesh and _outMesh */
  virtual ~TriangleWrapper();

  void setDomain( const Domain2D & domain );
  void setSwitches( const string & s );
  void generate( Mesh2D & outMesh );
  Mesh2D mesh();

//   int loadPolyFile( const string & fname );
//   void saveInputPolyFile( const string & fname );
//   void saveOutputPolyFile( const string & fname );
//  /*! Start triangulation */
//   int generateMesh();
//   /*! Set triangle-configuration */


protected:
  /*! For internal use only. */
  void init_( );

//   /*! For internal use only. */
  void transformToMesh_( const triangulateio & trimesh, Mesh2D & mesh );
  /*! For internal use only. */
  //  void transformFromMesh_( const BaseMesh & mesh, triangulateio & trimesh );
  void transformFromDomain_( const Domain2D & mesh, triangulateio & trimesh );
  
  /*! For internal use only. */
  void allocateOutMemory_();
  /*! For internal use only. */
  void freeMemory_();
  /*! For internal use only. */

  struct triangulateio * mesh_input_;
  struct triangulateio * mesh_output_;
  struct triangulateio * mesh_voronoi_output_;

  string switches_;
};


#endif // TRIANGLEWRAPPER__H

/*
$Log: trianglewrapper.h,v $
Revision 1.9  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.8  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.7  2005/01/17 18:17:54  carsten
*** empty log message ***

Revision 1.6  2005/01/14 12:51:27  carsten
*** empty log message ***

Revision 1.5  2005/01/14 11:40:24  carsten
*** empty log message ***

Revision 1.4  2005/01/06 19:46:00  carsten
*** empty log message ***

Revision 1.2  2005/01/06 17:07:42  carsten
*** empty log message ***

Revision 1.1  2005/01/06 16:15:56  carsten
*** empty log message ***

*/
