# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 10:26:14 2011

@author: Guenther.T
"""

import numpy as np
import pygimli as pg

dx = 2.
nb = 1
xmin, xmax = 0., 20.
ymin, ymax = 0., 32.5
zmax = 10.
x = pg.asvector(np.arange(xmin-dx*nb, xmax+dx*(nb+1), dx))
y = pg.asvector(np.arange(ymin-dx*nb, ymax+dx*(nb+1), dx))
z = pg.asvector(np.arange(-np.ceil(zmax / dx), 1.) * dx)
print(x, y, z)

mesh = pg.Mesh()
mesh.create3DGrid(x, y, z)
mesh2 = mesh.createH2()
mesh2.createNeighbourInfos()
bounds = []
for b in mesh2.boundaries():
    if b.leftCell() is None or b.rightCell() is None and b.center()[2] < 0.:
        bounds.append(b)
        b.setMarker(1)

print(mesh)
print(mesh2)
len(bounds)
mesh2.save('para')
mesh2.exportVTK('para')

poly = pg.Mesh()
poly.createMeshByBoundaries(mesh2, mesh2.findBoundaryByMarker(1))
poly.exportAsTetgenPolyFile('paraBoundary')
