# -*- coding: utf-8 -*-

#import wx
import sys
import os

import pygimli as g
import pybert as b

from pygimli.gui.base import AppResource
#from pygimli.gui.wxmpl import AppResourceWxMPL

#from pybert.importer import importData, bertImportDataFileSuffixesDict

#from .bertDataCreator import BertDataCreator
#from .bertDataHandler import BertDataHandler
#from .bertMeshHandler import BertMeshHandler
#from .bertModelHandler import BertModelHandler
#from .bertInversionProcess import BertInversionProcess

from threading import Timer

#import wx.lib.delayedresult as delayedresult

#try:
    #from agw import aui
    #from agw.aui import aui_switcherdialog as ASD
#except ImportError: # if it's not there locally, try the wxPython lib.
    #import wx.lib.agw.aui as aui
    #from wx.lib.agw.aui import aui_switcherdialog as ASD

#from pygimli.gui.resources import loadIcon, MakeDisabledBitmap

class BertApp(AppResource):
    def __init__(self, parent, rendererSlot, propertyInspectorSlot):
        super().__init__(parent, rendererSlot, propertyInspectorSlot)
        self.setName("BERT")
        #self.loadXRC('bertApp.xrc', __file__)
        
        #self.mbBERTDataMenu = self.createMainMenu('&Data')

        #self.createMenuItem(self.mbBERTDataMenu
                                    #, name = "&Import ... \tCtrl-I"
                                    #, function = self.onImportDataFile)

        #self.createMenuItem(self.mbBERTDataMenu
                                    #, name = "Create &new ...\tCtrl-N"
                                    #, function = self.onCreateData)
        #self.toolBar_ = None

        #self.dataHandler_  = None
        #self.meshHandler_  = None
        #self.modelHandler_ = None


        #self.parent.addCommandToOnIdleQueue(self.postCreate)
        #self.inversion_  = BertInversionProcess(useProcess = True)
        #self.ipcName_ = "pybert.gimli.inversion-" + str(wx.NewId())
        #self.ipc_ = g.IPCClientSHM()
       
        #self.invStatusTimer_ = wx.PyTimer(self.readInversionStatus)

        #self.running_    = False
        #self.jobID_      = 0
        #self.abortEvent_ = delayedresult.AbortEvent()
        #self.abortSet_   = False

    def processData(self):
        if not self.running_:
            didSomeThing = True
            if self.dataHandler_ is not None and self.modelHandler_ is not None:
                self.inversion_.setMesh(self.getSource('mesh'))
                self.inversion_.setData(self.getSource('data'))
                print("BertApp process", self.inversion_.paraDomain())
                self.modelHandler_.setMesh(self.inversion_.paraDomain())
            return didSomeThing
        else:
            return False

    def postCreate(self):
        pass

    def readInversionStatus(self):
        if not self.running_:
            return

        self.ipc_.info()
        print("Iter: ", self.ipc_.getInt('Iter'), self.ipc_.getDouble('Chi2'))

    def getApplicationToolBar(self, parent):
        if not self.toolBar_:
            self.toolBar_ = aui.AuiToolBar(self.parent, -1, wx.DefaultPosition, wx.DefaultSize,
                                        aui.AUI_TB_DEFAULT_STYLE | aui.AUI_TB_PLAIN_BACKGROUND)

            self.toolBar_.SetName("BERT Toolbar")

            self.toolBar_.SetToolBitmapSize(wx.Size(22, 22))

            self.tbSave = self.toolBar_.AddSimpleTool(wx.NewId(), "Save", loadIcon("document-save.png")
                                            , "Save current project")
            self.tbSave.SetDisabledBitmap(MakeDisabledBitmap(loadIcon("document-save.png")))

            self.tbSaveAs = self.toolBar_.AddSimpleTool(wx.NewId(), "Save as", loadIcon("document-save-as.png")
                                            , "Save current project and select filename")
            self.tbSaveAs.SetDisabledBitmap(MakeDisabledBitmap(loadIcon("document-save-as.png")))
            self.toolBar_.AddSeparator()

            self.tbRun = self.toolBar_.AddSimpleTool(wx.NewId(), "Run inversion", loadIcon("arrow-right.png")
                                            , "Start inversion of the current profile")
            self.tbRun.SetDisabledBitmap(MakeDisabledBitmap(loadIcon("arrow-right.png")))

            self.tbAbort = self.toolBar_.AddSimpleTool(wx.NewId(), "Abort inversion", loadIcon("process-stop.png")
                                            , "Abort inversion of the current profile")
            self.tbAbort.SetDisabledBitmap(MakeDisabledBitmap(loadIcon("process-stop.png")))

            self.toolBar_.EnableTool(self.tbAbort.GetId(), False)
            self.toolBar_.EnableTool(self.tbRun.GetId(), True)
            self.toolBar_.EnableTool(self.tbSave.GetId(), False)
            self.toolBar_.EnableTool(self.tbSaveAs.GetId(), False)

            wx.EVT_TOOL(self.toolBar_, self.tbRun.GetId(), self.onInvertStartButton)
            wx.EVT_TOOL(self.toolBar_, self.tbAbort.GetId(), self.onInvertAbortButton)
            wx.EVT_TOOL(self.toolBar_, self.tbSave.GetId(), self.onSaveButton)
            wx.EVT_TOOL(self.toolBar_, self.tbSaveAs.GetId(), self.onSaveAsButton)

        return self.toolBar_
    
    def onCreateData(self, event = None):
        """Add application for data creation. """
        creator = self.createSubPanel(BertDataCreator)
        creator.activate(True)
        
    def onInvertStartButton(self, event = None):
        ''
        ''
        ''
        if not self.running_:
            self.jobID_ += 1
            print(("Starting job %s " % self.jobID_))
            self.abortEvent_.clear()
            self.abortSet_ = False;

            self.toolBar_.EnableTool(self.tbAbort.GetId(), True)
            self.toolBar_.EnableTool(self.tbRun.GetId(), False)
            self.toolBar_.Refresh()

            self.process()

            self.ipc_.setSegmentName(self.ipcName_)
            self.inversion_.inv.ipc().setSegmentName(self.ipcName_)

            #self.inversion_.inv_.ipc().connect(self.parent.ws.ipcServer.server_address[0], self.parent.ws.ipcServer.server_address[1])

            self.invStatusTimer_.Start(100)
            self.running_ = True
            delayedresult.startWorker(self.inversionFinish, self.inversionStart,
                                        wargs = (self.jobID_, self.abortEvent_), jobID = self.jobID_)

    def onInvertAbortButton(self, event = None):
        pass

    def inversionStart(self, jobID = 0, abortEvent = None):
        while len(self.parent.onIdleCmdQueue_) > 0:
            #print len(self.parent.onIdleCmdQueue_)
            time.sleep(0.1)

        self.inversion_.start()

    def inversionFinish(self, delayedresult = None):
        self.invStatusTimer_.Stop()
        try:
            delayedresult.get()
        except Exception as e:
            sys.stderr.write("BertApp::inversionFinish() " + str(e))

        self.running_ = False
        self.toolBar_.EnableTool(self.tbAbort.GetId(), False)
        self.toolBar_.EnableTool(self.tbRun.GetId(), True)
        self.toolBar_.Refresh()

        self.modelHandler_.setModel(self.inversion_.model())

        print(self.ipc_.getInt('Iter'))
        self.ipc_.free(self.ipcName_)

    def onSaveButton(self, event = None):
        pass

    def onSaveAsButton(self, event = None):
        pass

    def onImportDataFile(self, event = None):
        """ Open file dialog and import a data file. """
        wildcard = "All supported data formats |"
        for suffix in list(bertImportDataFileSuffixesDict.keys()):
            wildcard += '*' + suffix + ';'
        wildcard += '|'

        for suffix in list(bertImportDataFileSuffixesDict.keys()):
            wildcard += bertImportDataFileSuffixesDict[ suffix ][0] + "|*" + suffix + "|"
        wildcard += "All files (*.*)|*.*"

        dlg = wx.FileDialog(self.parent, message = "Open data file",
                            defaultDir = os.getcwd(),
                            defaultFile = "",
                            wildcard = wildcard,
                            style = wx.OPEN | wx.CHANGE_DIR | wx.FD_MULTIPLE)

        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            print(('You selected %d files:' % len(paths)))

            self.openFile(paths[ 0 ])

        print(("CWD: %s\n" % os.getcwd()))

        dlg.Destroy()
    # def onImportDataFile(...)
    
    def openFile(self, paths = None):
        ''
        ' Load data file and create data window '
        ''
        if paths is None:
            print("openDataFile: no valid filename given")
            return

        fileName = None
        if type(paths) is list:
            fileName = paths[0]
        else:
            fileName = paths

        data = None
        try:
            #data = g.DataContainerERT(g.utils.unicodeToAscii(fileName))
            print("open", g.utils.unicodeToAscii(fileName))
            data = importData(g.utils.unicodeToAscii(fileName))
        except Exception as e:
            err = wx.MessageDialog(self.parent
                                    , 'BERT datafile: ' + fileName + '\n'
                                       + 'loading failed for the following reason: \n' + str(e)
                                    , 'Something goes wrong while opening file.'
                                    , wx.OK | wx.ICON_WARNING)
            err.ShowModal()

        if data is not None:
            self.dataHandler_ = self.createSubPanel(BertDataHandler)
            self.dataHandler_.setData(data, dataName=fileName)
            self.parent.resourceTree.SetItemText(self.dataHandler_.treeItem, fileName)
            
            self.meshHandler_ = self.createSubPanel(BertMeshHandler)
            self.meshHandler_.addDependency(self.dataHandler_)
            self.meshHandler_.setSource('data', self.dataHandler_.getData)

            self.addDependency(self.dataHandler_)
            self.addDependency(self.meshHandler_)
            self.setSource('data', self.dataHandler_.getData)
            self.setSource('mesh', self.meshHandler_.getParaMesh)

            self.modelHandler_ = self.createSubPanel(BertModelHandler)
            self.modelHandler_.setSource('paraDomain', self.inversion_.paraDomain)
    # def openDataFile(...)