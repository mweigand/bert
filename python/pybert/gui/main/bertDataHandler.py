# -*- coding: utf-8 -*-

import wx

import pygimli as g
import pybert as b

from pygimli.gui.base import ManagedProperties
    
from pygimli.gui.wxmpl import AppResourceWxMPL
from pygimli.gui.wxmpl.colorBarWxMPL import ColorBarWxMPL
from pygimli.gui.resources import loadIcon

from pygimli.mplviewer import *
from pybert.dataview import *

import numpy as np

from matplotlib.patches import Circle, Ellipse
from matplotlib.collections import PatchCollection

import operator

class BertDataFilterRule(ManagedProperties):
    def __init__(self, parent, panel):
        ManagedProperties.__init__(self)
        
        self.parent = parent
        
        self.operator1 = None
        self.value1 = None
        self.operator2 = None
        self.value2 = None

        
        self.sizer = wx.xrc.XRCCTRL(panel.GetParent(), 'hackFilterBoxFinder').GetContainingSizer()
        self.rulePane = parent.xrc.LoadPanel(panel,  "DataFilterTemplate")
        
        self.sizerItem = self.sizer.Insert(len(self.sizer.GetChildren())-1, self.rulePane, 1, wx.ALL|wx.EXPAND)
        
        self.fieldDataProp = self.appendProperty("FieldData", default = "None", valType = str)
        self.fieldDataProp.setCtrl(ctrl = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleFieldChoise')
                                        , ctrlEvent = wx.EVT_CHOICE
                                        , targetFunct = self.onChangeFieldProp)  
        self.fieldDataProp.ctrl.SetMaxSize(wx.Size(65, -1))
        self.fieldDataProp.ctrl.SetMinSize(wx.Size(65, -1))
        
                                        
        self.op1Prop = self.appendProperty("Op1", default = '<', valType = str)
        self.op1Prop.setCtrl(ctrl = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleOperatorChoise')
                                        , ctrlEvent = wx.EVT_CHOICE
                                        , targetFunct = self.update)
        self.op1Prop.ctrl.SetMaxSize(wx.Size(40, -1))
        self.op1Prop.ctrl.SetMinSize(wx.Size(40, -1))
        
        self.value1Prop = self.appendProperty("Value1", default = 0., valType = float)
        self.value1Prop.setCtrl(ctrl = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleValueTextCtrl')
                                        , ctrlEvent = wx.EVT_KILL_FOCUS
                                        , targetFunct = self.update)             
       
        self.addProp = self.appendProperty("add", default = 'None', valType = str)
        self.addProp.setCtrl(ctrl = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleOperatorAddChoise')
                                        , ctrlEvent = wx.EVT_CHOICE
                                        , targetFunct = self.onChangeAddChoise)
        self.addProp.ctrl.SetMaxSize(wx.Size(60, -1))
        self.addProp.ctrl.SetMinSize(wx.Size(60, -1))
        
        self.op2Prop = self.appendProperty("Op2", default = '>', valType = str)
        self.op2Prop.setCtrl(ctrl = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleOperator2Choise')
                                        , ctrlEvent = wx.EVT_CHOICE
                                        , targetFunct = self.update)
        self.op2Prop.ctrl.SetMaxSize(wx.Size(40, -1))
        self.op2Prop.ctrl.SetMinSize(wx.Size(40, -1))
        
        self.value2Prop = self.appendProperty("Value2", default = 0., valType = float)
        self.value2Prop.setCtrl(ctrl = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleValue2TextCtrl')
                                        , ctrlEvent = wx.EVT_KILL_FOCUS
                                        , targetFunct = self.update)             
                                        
        
        try:
            button = wx.xrc.XRCCTRL(self.rulePane, 'DataFilterRuleDelButton')
            button.SetBitmapLabel(loadIcon('edit-delete.png'))
            button.Bind(wx.EVT_BUTTON, self.deleteRule)         
        
            fields = []
            fields.append('None')
            for key in list(self.parent.data.dataMap().keys()):
                if self.parent.data.haveData(key):
                    fields.append(key + " " + self.parent.data.dataDescription(str(key)))
        
            self.fieldDataProp.ctrl.AppendItems(fields)
            self.fieldDataProp.setVal('None')
            
            #self.fieldDataProp.ctrl.Layout()

        except Exception as e:
            print(e)
            pass
        
        self.parent.propertyPanel_.GetPage(0).Layout()
        
    # def __init__(...)

    
    def deleteRule(self, event = None):
        self.sizer.Hide(self.rulePane)
        self.sizer.Detach(self.rulePane)
        self.parent.propertyPanel_.GetPage(0).Layout()
        self.parent.filterRules.remove(self)
        self.update()
        
    #def deleteRule(...)
        
    def apply(self, data):
        '''
            Apply this rule in the given data and mark all matching date as invalid
        '''
        
        fieldname = str(self.fieldDataProp().split()[0])
        
        if not fieldname == 'None':
            print("Rule:", fieldname, self.op1Prop(), self.value1Prop())
            
            invalid = None
            op1 = None
            if (self.op1Prop() == '<'):   op1 = operator.__lt__
            elif (self.op1Prop() == '>'): op1 = operator.__gt__
            elif (self.op1Prop() == '='): op1 = operator.__eq__
            
            invalid = op1(data.get(fieldname), self.value1Prop())
            
            op2 = None
            if (self.op2Prop() == '<'):   op2 = operator.__lt__
            elif (self.op2Prop() == '>'): op2 = operator.__gt__
            elif (self.op2Prop() == '='): op2 = operator.__eq__
                
            if self.addProp() == 'and':
                invalid = invalid & op2(data.get(fieldname), self.value2Prop())
            elif self.addProp() == 'or':
                invalid = invalid | op2(data.get(fieldname), self.value2Prop())
                
            idx = g.find(invalid)
            print("Filter:", len(idx), "values")
            data.markInvalid(idx)
    
    def onChangeAddChoise(self, event = None):
        if not self.addProp() == 'None':
            self.op2Prop.ctrl.Hide(True)
            self.value2Prop.ctrl.Hide(True)
        else:
            self.op2Prop.ctrl.Hide(False)
            self.value2Prop.ctrl.Hide(False)
            
        self.update()
    
    def onChangeFieldProp(self, event = None):
        print("update Field:", self.fieldDataProp())
        self.update()
        
    def update(self):
        '''
            Call parent datahandler to apply the filter
        '''
        
        self.parent.filterData()
    #def update(...)
        

class BertDataHandler(AppResourceWxMPL):
    """Holds the data. Manage visualisation and editing"""
    
    def __init__(self, parent, rendererSlot, propertyInspectorSlot):
        AppResourceWxMPL.__init__(self, parent, rendererSlot, propertyInspectorSlot)
        self.setName("Data")
        self.piName_    = "Data"
        self.data       = None
        self.values_    = None
        self.dataName   = None
        self.filterRules = []
        
        self.cbar = ColorBarWxMPL(self)
                
        self.electrodeMarker    = None
        self.dataMarker         = None
        self.markerHighlights  = None

        self.pseudoTypeProp = self.appendProperty("Pseudotype", default = "unknown", valType = str)
        self.fieldDataProp = self.appendProperty("FieldData", default = "None", valType = str)
        self.viewStyleProp = self.appendProperty("ViewStyle", default = 0, valType = int)
  
        self.mbData = self.findMainMenu('Data')
        menuSection = self.createLocalMenuSection(self.mbData)
        menuSection.addItem(name = "&Export ...\tCtrl-E", function = self.onExport)

    def getData(self): return self.data

    def processData(self):
        didSomeThing = False
        return didSomeThing

    def setData(self, data, dataName='data'):
        self.data = data
        self.dataName = dataName
        
        self.values_ = g.RVector(self.data.size(), 0.0)

        if self.data.haveData('rhoa'):
            print("show rhoa")
            self.values_ = self.data.get('rhoa')
        elif self.data.haveData('r'):
            print("show r * k")
            self.values_ = self.data.get('r') * b.geometricFactor(self.data)
            self.data.set('rhoa', self.values_)
        elif self.data.haveData('u') and self.data.allNonZero('i'):
            print("show u/i * k")
            self.values_ = self.data.get('u') / self.data.get('i') * b.geometricFactor(self.data)
            self.data.set('rhoa', self.values_)
        print("values: ", min(self.values_), max(self.values_))
        
        self.data.markValid(g.find(self.data('rhoa') == 0.0), False)

        # can be set to true after the propPanel is activated
        #self.cbar.active = True 
        self.draw()

    def filterData(self):
        '''
            Called from filter rules
        '''
        
        swatch = g.Stopwatch(True)
        
        self.data.markValid(list(range(self.data.size())))
        
        for rule in self.filterRules:
            rule.apply(self.data)
        
        print("filter time", swatch.duration(), 's')
        
        self.draw()
    # def filterData(...)
        
        
    def createPropertyPanel(self, parent):
        '''
            This is an interface method
            the resulting panel can be found as self.propertyPanel_
        '''
        panel = self.createPropertyInspectorNoteBookPanel(parent, 'piBertDataHandler', title = self.piName_)

        panel.AddPage(self.cbar.createPropertyPanel(panel), "Colorbar", False)
        
        self.pseudoTypeProp.setCtrl(ctrl = wx.xrc.XRCCTRL(panel, 'PseudoTypeChoice') # name of the control in xrc
                                        , ctrlEvent = wx.EVT_CHOICE                      # the event that should observed
                                        , targetFunct = self.draw)                  # the callback when the event is called

        self.fieldDataProp.setCtrl(ctrl = wx.xrc.XRCCTRL(panel, 'Bert/DataHandler/FieldChoice') # name of the control in xrc
                                        , ctrlEvent = wx.EVT_CHOICE                      # the event that should observed
                                        , targetFunct = self.setFieldData)                  # the callback when the event is called

        self.viewStyleProp.setCtrl(ctrl = wx.xrc.XRCCTRL(panel, 'Bert/DataHandler/ViewStyleRadioBox') # name of the control in xrc
                                        , ctrlEvent = wx.EVT_RADIOBOX                                   # the event that should observed
                                        , targetFunct = self.draw)                  # the callback when the event is called

        try:
            button = wx.xrc.XRCCTRL(panel, 'DataFilterAddButton')
            button.SetBitmapLabel(loadIcon('list-add.png'))
            button.Bind(wx.EVT_BUTTON, self.addDataFilterRule)                                        
          
            types = []
            
            for att in dir(Pseudotype):
                if att[0] != '_':
                    types.append(att)
            
            self.pseudoTypeProp.ctrl.AppendItems(types)
            self.pseudoTypeProp.setVal('unknown')

            fields = []
            fields.append('None')
            for key in list(self.data.dataMap().keys()):
                if self.data.haveData(key):
                    fields.append(key + " " + self.data.dataDescription(str(key)))
        
            self.fieldDataProp.ctrl.AppendItems(fields)
            self.fieldDataProp.setVal('None')
        except:
            pass
        
        # set default toolbar setting
        # auto aspect is a good idea here
        self.getToolBar().setAspectAuto(True)
        
        return panel

    def setFieldData(self):
        token = self.fieldDataProp().split()[0]
        if self.data.haveData(str(token)):
            self.values_ = self.data(str(token))
            self.parent.statusBar.setStatusMessage("Field data: " + token + ", min:" + str(min(self.values_)) + " max: " + str(max(self.values_)), 0)
            self.cbar.label.setVal(token)
            self.draw()
        else:
            print("no data for ", self.fieldDataProp())

    def addDataFilterRule(self, event = None):
        '''
            Create a new data filter panel and add them to the filter queue
        '''
        self.filterRules.append(BertDataFilterRule(self, event.GetEventObject().GetParent()))
        
    #def addDataFilter()
        
    def onMousePress(self, event):
        """ Bind by parent class """
        
        print("mousePress")
        axes = event.inaxes
        if axes is not None:
            print(axes)
            print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f'%(
            event.button, event.x, event.y, event.xdata, event.ydata))

            p = axes.pick(event)
            print("inaxes pick", p)

        else:
            #print event,event.name,event.canvas
            #p = event.canvas.pick(event)
            #print "canvas pick", p
            #p = event.canvas.figure.pick(event)
            #print "figure pick", p
            #for k in self.axes.spines.keys():
                #p = self.axes.spines[k].pick(event)
                #print "spine pick", k, p
            print("Pos:", event.canvas._lastx, event.canvas._lasty)
            xdata, ydata = self.axes.transData.inverted().transform_point((event.canvas._lastx, event.canvas._lasty))
            print("DPos:", xdata, ydata)

    def onPick(self, event):
        """ Bind by parent class """
        
        artist = event.artist
        ind = event.ind
        
        for idx in ind:
            idx = int(idx)
            if artist == self.electrodeMarker: 
                self.highLightElectrode(idx)
            elif artist == self.dataMarker: 
                self.highLightData(idx)
    #END onPick(...)
        
    def prephighLight(self):
        """ calculate highlight dimension """
        
        if self.markerHighlights:
            if self.markerHighlights in self.axes.collections:
                self.axes.collections.remove(self.markerHighlights)

        dx1, dy1 = self.axes.transData.inverted().transform_point((0.0, 0.0))
        dx2, dy2 = self.axes.transData.inverted().transform_point((10.0, 10.0))
        diamx = dx2 - dx1
        diamy = dy2 - dy1
        
        return diamx, diamy
        
    def highLightElectrode(self, idx):
        """ Highlight single electrode and all associated data"""
                
        patches = []
        diam = self.prephighLight()

        # add the data that are associated to the selected electrode
        
        aid = (self.data('a') == idx) | (self.data('b') == idx)
        mid = (self.data('m') == idx) | (self.data('n') == idx)
        ids = np.asarray(g.find(aid | mid))
       
        xyo = list(zip(self.dataMarker.get_xdata()[ ids ], self.dataMarker.get_ydata()[ ids ]))

        colors = []
        for p in xyo:
            circle = Ellipse(p, diam[0], diam[1])
            colors.append((0.0, 0.0, 1.0))
            patches.append(circle)
        
        # add the electrode itself
        circle = Ellipse((self.data.sensorPositions()[ idx ][ 0 ], self.data.sensorPositions()[ idx ][ 1 ]), diam[0], diam[1])
        colors.append((0.0, 1.0, 0.0))
        patches.append(circle)
        
        self.markerHighlights = PatchCollection(patches, alpha = 0.5, color = colors)
        self.axes.add_collection(self.markerHighlights)
        
        self.updateDrawOnIdle()
        
    def highLightData(self, idx):
        ''
        ' highlight single data and all associated electrodes'
        ''
        print("data: ", idx, self.data('a')[idx], self.data('b')[idx], self.data('m')[idx], self.data('n')[idx])
        
        diam = self.prephighLight()

        # add the data that are associated to the selected electrode
        
        ids = [ self.data('a')[idx], self.data('b')[idx], self.data('m')[idx], self.data('n')[idx] ]
        patches = []
        colors = []
        
        for i, ei in enumerate(ids):
            if ei > -1:
                if i < 2:
                    colors.append((1.0, 0.0, 0.0))
                else:
                    colors.append((0.0, 0.0, 1.0))
                p = self.data.sensorPositions()[ int(ei) ]
                patches.append(Ellipse((p[0], p[1]), diam[0], diam[1]))
        
        # add the electrode itself
        circle = Ellipse((self.dataMarker.get_xdata()[ idx ], self.dataMarker.get_ydata()[ idx ]), diam[0], diam[1])
        colors.append((0.0, 1.0, 0.0))
        patches.append(circle)
        
        self.markerHighlights = PatchCollection(patches, alpha = 0.5, color = colors)
        self.axes.add_collection(self.markerHighlights)
        
        self.updateDrawOnIdle()
        
    
    def onExport(self, event=None):
        """
            Open file dialog to export the current active data
        """
        exportName = self.dataName[0:self.dataName.find('.')]
        
        filename = self.getExportFileNameWithDialog(exportName + ".dat", 
            "GIMLi unified data format (*.dat)|*.dat|All files (*.*)|*.*")
        
        if filename:
            self.data.save(filename)
    #def onExport(...)
    
    def drawData_(self):
        '''
            This is an interface method.
            Define what we have to be drawn (needed from base class) is called while a draw event is fired
        '''
         
        print("redraw all data") 
        
        if self.data is not None:
            if self.viewStyleProp() == 0:
                # draw Matrix
                self.gci = drawDataAsMatrix(self.axes, self.data, self.values_, getattr(Pseudotype, self.pseudoTypeProp()))
            elif self.viewStyleProp() == 1:
                # draw Patch
                self.gci = drawDataAsPatches(self.axes, self.data, self.values_, getattr(Pseudotype, self.pseudoTypeProp()))
            elif self.viewStyleProp() == 2:
                # draw Configuration
                self.electrodeMarker, self.dataMarker = drawDataAsMarker(self.axes, self.data, schemetype = getattr(Pseudotype, self.pseudoTypeProp()));

        if self.cbar.active:
            self.cbar.activator(False)
            self.cbar.activator(True)
            
    # def drawData_(...)
    
