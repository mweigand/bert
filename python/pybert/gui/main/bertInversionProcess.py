#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

import pygimli as g
import pybert as b

from multiprocessing import Process, Manager

import numpy as np

class BertInversionProcess( ):
    ''
    ' BERT Inversion handler.'
    ''
    def __init__( self, useProcess = False, verbose = True ):
        self.verbose = verbose
        self.useProcess_ = useProcess
        print("manager start")
        try:
            self.manager_ = Manager()
            self.workspace_ = self.manager_.Namespace()
        except:
            self.manager = None
            self.useProcess_ = False

        print("manager end")
        

        self.fop = b.DCSRMultiElectrodeModelling( verbose )
        self.inv = g.RInversion( verbose, verbose )
        # TODO: this should the right place here, but fails without mesh
        #self.inv.setForwardOperator( self.fop )

        self.transData = g.RTransLog()
        self.transModel = g.RTransLog()
        self.inv.setTransData( self.transData )
        self.inv.setTransModel( self.transModel )

        self.paraDomain_ = None
        g.savePythonGIL( False )
        
    def setMesh( self, mesh ):
        ''
        ' Set a mesh. Allways set mesh before data.'
        ''
        self.fop.setMesh( g.Mesh( mesh ) )

        self.fop.regionManager().region( list(self.fop.regionManager().regions().keys())[0] ).setBackground( True );

        # TODO das ist zwingend notwendig damit self.fop.mesh.marker richtig numeriert sind, MUSS DAS SEIN???
        #print "fop.mesh()", min(self.fop.mesh().cellMarker()), max(self.fop.mesh().cellMarker())
        self.fop.createRefinedForwardMesh( True, False )
        #print "fop.mesh()", min(self.fop.mesh().cellMarker()), max(self.fop.mesh().cellMarker())

        self.paraDomain_ = g.Mesh( self.fop.regionManager().paraDomain() )
        
        self.inv.setForwardOperator( self.fop )

    def setData( self, data ):
        ''
        ' Set a valid data. rhoa and error have to been set correctly. Allways set mesh before data.'
        ''
        #self.fop.setData( data )

        self.data = b.DataContainerERT( data )
        self.fop.setData( self.data )
        
        self.inv.setData( self.data.get( 'rhoa' ) )
        self.inv.setError( self.data.get( 'err' ) )
        
        startModel = g.RVector( self.fop.regionManager().parameterCount(), g.median( data.get( 'rhoa' ) ) )

        self.inv.setModel( startModel )
        self.inv.setLambda( 20 )

    def paraDomain( self ):
        ''
        ' Return the current paraDomain.'
        ''
        return self.paraDomain_

    def model( self ):
        ''
        ' Return the current model. '
        ''
        return g.asvector( self.workspace_.model )

    def start( self ):
        self.workspace_.error = ''
        self.workspace_.model = []
        
        if self.useProcess_:
            #Process._bootstrap = myBootstrap
            print("create worker")
            worker = Process( target = self.run_, args = ( self.workspace_, self.inv, ) )

            try:
                worker.start()
            except Exception as e:
                self.workspace_.error = "Process start: " + str( sys.exc_info()[0] ) + str( e )
                #import traceback
                #traceback.print_exc()

            notintr = False
            while not notintr:
                try:
                    worker.join()
                    notintr = True
                except Exception as e:
                    self.workspace_.error = "Process join: " + str( sys.exc_info()[0] ) + str( e )

            if len( self.workspace_.error ) > 0:
                raise Exception( self.workspace_.error )
        else:
            self.run_( self.workspace_, self.inv )

    def run_( self, ws, inv ):
        ''
        ' Internal. call start() to run inversion.'
        ''
        try:
            print("running")
            sys.stdout.flush()
            inv.run()
            ws.model = np.asarray( inv.model() )
        except Exception as e:
            ws.error = "Process run:" + str( sys.exc_info()[0] ) + str( e )
            ws.model = [ ]
            print(ws.error)


def dcinv( data, mesh ):
    ''
    ' For testing only. '
    ''
    #g.savePythonGIL( True )
    fop = b.DCSRMultiElectrodeModelling( True )
    inv = g.RInversion( True, True )
    transData = g.RTransLog( )
    transModel = g.RTransLog( )
    inv.setTransData( transData )
    inv.setTransModel( transModel )

    fop.setMesh( mesh )
    fop.regionManager().region( list(fop.regionManager().regions().keys())[0] ).setBackground( True );
    fop.createRefinedForwardMesh( False, False );
    paraDomain = fop.regionManager().paraDomain();
    inv.setForwardOperator( fop )

    fop.setData( data )
    inv.setData( data.get( 'rhoa' ) )
    inv.setError( data.get( 'err' ) )
    inv.setModel( g.RVector( fop.regionManager().parameterCount(), g.median( data.get( 'rhoa' ) ) ) )

    inv.setMaxIter( 1 )
    inv.run()
# def dcinv( ... )


def test( argv ):
    from optparse import OptionParser

    parser = OptionParser( "usage: %prog [options] data", version = "%prog: " + g.versionStr()  )
    parser.add_option("-v", "--verbose", dest = "verbose", action = "store_true"
                            , help = "be verbose", default = False )
    parser.add_option("-p", "--mesh", dest = "meshFileName"
                            , help = "a valid mesh for inversion.", metavar = "File" )

    (options, args) = parser.parse_args()

    if options.verbose:
        print(options, args)
        __verbose__ = True

    if len( args ) == 0:
        parser.print_help()
        print("Please add a datafile")
        sys.exit( 2 )
    else:
        dataFileName = args[ 0 ]


    data = b.DataContainerERT( dataFileName )
    mesh = g.Mesh( options.meshFileName )

    #dcinv( data, mesh )

    inv = BertInversionProcess( useProcess = False, verbose = options.verbose )

    inv.setMesh( mesh )
    inv.setData( data )

    if options.verbose:
        print("data:", data)
        print("mesh:", mesh)
        print("paramesh:", inv.paraDomain())

    #inv.inv.run()
    inv.start( )

# def test( ... )
    
if __name__ == "__main__":
    test( sys.argv[ 1: ] )