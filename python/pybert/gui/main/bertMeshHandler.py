# -*- coding: utf-8 -*-

import wx

import pygimli as g
import pybert as b

from pygimli.gui.wxmpl import AppResourceWxMPL
from pygimli.gui.resources import loadIcon
from pygimli.mplviewer import drawMesh, drawSensors
from pygimli.meshtools import appendTriangleBoundary, createParaMesh2dGrid

class BertMeshHandler( AppResourceWxMPL ):
    '''
        Create ParaMeshes
    '''
    def __init__( self, parent, rendererSlot, propertyInspectorSlot ):
        AppResourceWxMPL.__init__( self, parent, rendererSlot, propertyInspectorSlot )
        self.setName( "Mesh" )
        self.piName_ = "Parametrization"

        self.electrodes_ = None
        self.mesh = None
        self.poly_ = None

        self.gridPanel_ = None
        self.meshPanel_ = None
        self.paraDepth_ = 0.0

        self.styleProp = self.appendProperty( "paraStyle", valType = str, default = 'Mesh' )

        self.paraDepthProp = self.appendProperty( "paraDepth", valType = float, default = 0.0 )
        self.gridLayers = self.appendProperty( "gridLayers", valType = int, default = 11 )
        self.gridDX = self.appendProperty( "gridDX", valType = float, default = 1 )
        self.gridDZ = self.appendProperty( "gridDZ", valType = float, default = 1 )

    def getParaMesh( self ): return self.mesh

    def createPropertyPanel( self, parent ):
        panel = self.createPropertyInspectorNoteBookPanel( parent, 'piBertParaMeshHandler', title = self.piName_ )

        self.styleProp.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'paraMeshStyleRadioBox' )
                                        , ctrlEvent = wx.EVT_RADIOBOX
                                        , targetFunct = self.setParaStyle )

        self.gridLayers.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'gridLayersTextCtrl' )
                                        , ctrlEvent = wx.EVT_KILL_FOCUS
                                        , targetFunct = self.process )
        self.gridDX.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'gridDXTextCtrl' )
                                        , ctrlEvent = wx.EVT_KILL_FOCUS
                                        , targetFunct = self.process )
        self.gridDZ.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'gridDZTextCtrl' )
                                        , ctrlEvent = wx.EVT_KILL_FOCUS
                                        , targetFunct = self.process )

        self.paraDepthProp.setCtrl( ctrl = wx.xrc.XRCCTRL( panel, 'paraDepthTextCtrl')
                                       , ctrlEvent = wx.EVT_KILL_FOCUS
                                       , targetFunct = self.process )

        button = wx.xrc.XRCCTRL( panel, 'paraDepthAutoButton' )
        button.SetBitmapLabel(loadIcon( 'system-run.png' ) )
        button.Bind( wx.EVT_BUTTON, self.onParaDepthAuto )

        self.gridPanel_ = wx.xrc.XRCCTRL( panel, 'gridPanel' )
        self.meshPanel_ = wx.xrc.XRCCTRL( panel, 'meshPanel' )

        return panel

    def processData( self ):
        print("BertParaMeshHandler::processData()")
        didSomeThing = True
        self.createParametrization()
        return didSomeThing

    def onParaDepthAuto( self, event = None ):
        self.paraDepthProp.setVal( 0.0 )
        self.process()

    def getParaDepth( self, data  ):
        '''
            Return paradepth from property or calculate automatically
        '''
        paradepth = self.paraDepthProp()

        if paradepth <= 0.0:
            paradepth = int( 10.0 * b.DCParaDepth( data ) ) / 10.0
            self.paraDepthProp.setVal( paradepth )
        
        return paradepth

    def setParaStyle( self, value = None ):
        if self.styleProp() == 'Grid':
            self.meshPanel_.Hide()
            self.gridPanel_.Show()
        else:
            self.gridPanel_.Hide()
            self.meshPanel_.Show()

        self.gridPanel_.GetParent().GetSizer().Layout()
        self.process( )

    def createParametrization( self ):
        if self.styleProp() == 'Grid':
            data = self.getSource( 'data' )
            self.mesh = createParaMesh2dGrid(   data.sensorPositions(),
                                    paraDX = self.gridDX(), 
                                    paraDZ = self.gridDZ(), 
                                    paraDepth = self.getParaDepth( data ), 
                                    nLayers = self.gridLayers(), 
                                    boundary = -1, 
                                    paraBoundary = 2, 
                                    verbose = False, 
                                    smooth = True )
        else:
            self.createParaMesh()

        self.setStatusMessage( "Created " + str( self.mesh.cellCount() ) + " parameter cells" )
            
        self.draw()

    #def createParaGrid( self ):
        #print "createParaGrid( self, value = None ):"
        #data = self.getSource( 'data' )
        #self.electrodes_ = data.sensorPositions();

        #eSpacing = self.electrodes_[ 0 ].distance( self.electrodes_[ 1 ] )

        #xmin = self.electrodes_[ 0 ].x() - 3.0 * eSpacing
        #xmax = self.electrodes_[ -1 ].x() + 3.0 * eSpacing

        #dx = eSpacing * self.gridDX()
        #dz = eSpacing * self.gridDZ()
        
        #x = g.utils.grange( xmax, xmin, dx = dx )

        #y = -g.increasingRange( dz, self.getParaDepth( data ), self.gridLayers() )

        #self.gridLayers.setVal( len( y )-1 )

        #self.mesh = g.Mesh()
        #self.mesh.createGrid( x, y )
        
        #self.setStatusMessage( "Created " + str( self.mesh.cellCount() ) + " parameter cells" )
        
        #map( lambda cell: cell.setMarker( 2 ), self.mesh.cells() )

        #self.paraXLimits = [ xmin, xmax ]
        #self.paraYLimits = [ min( y ), max( y ) ]
        #boundary = ( self.paraXLimits[ 1 ] - self.paraXLimits[ 0 ] ) * 2.0
        #self.mesh = g.meshtools.appendTriangleBoundary( self.mesh, boundary, boundary )
        

    def createParaMesh( self ):
        print("createParaMesh( self, value = None ):")
        data = self.getSource( 'data' )
        self.sensors = data.sensorPositions();

        #self.mesh = g.Mesh( 'dat/mesh.bms' )
        #return
        eSpacing = self.sensors[ 0 ].distance( self.sensors[ 1 ] )

        epos = self.sensors
        xmin = epos[0][0]; ymin = epos[0][1]; zmin = epos[0][2]
        xmax = epos[0][0]; ymax = epos[0][1]; zmax = epos[0][2]
        
        for e in epos:
            xmin = min( xmin, e[0] ); xmax = max( xmax, e[0] )
            ymin = min( ymin, e[1] ); ymax = max( ymax, e[1] )
            zmin = min( zmin, e[2] ); zmax = max( zmax, e[2] )

        #print xmin, xmax, ymin, ymax, zmin, zmax

        #if abs( zmax - zmin ) > 1e-12:
            #raise Exception( "Assuming 3D data: " + str( zmin) + " " + str( zmax ) )

        #eDepthMax = ymin; eDepthMin = ymax
        #paraDepth = abs( eDepthMax - eDepthMin )
        paraBoundary = eSpacing * 2.5
        top = 0.0;
        bottom = top - self.getParaDepth( data )

        if self.poly_ is None:
            self.poly_ = g.Mesh()
        else:
            self.poly_.clear()

        if self.mesh is None:
            self.mesh = g.Mesh()
        else:
            self.mesh.clear()

        paraDomain = []
        paraDomain.append( self.poly_.createNode( g.RVector3( xmin - paraBoundary, top ) ) )
        paraDomain.append( self.poly_.createNode( g.RVector3( xmin - paraBoundary, bottom ) ) )
        paraDomain.append( self.poly_.createNode( g.RVector3( xmax + paraBoundary, bottom ) ) )
        paraDomain.append( self.poly_.createNode( g.RVector3( xmax + paraBoundary, top ) ) )

        for i in range( len( epos ) ):
            paraDomain.append( self.poly_.createNode( epos[ len( epos ) -i -1 ] ) );

        self.paraXLimits = [ xmin - paraBoundary, xmax + paraBoundary ]
        self.paraYLimits = [ bottom, top ]

        for i, n in enumerate( paraDomain ):
            e = self.poly_.createEdge( n, paraDomain[ (i + 1) % len(paraDomain) ], 1 )
            if e.shape().center()[1] == 0.0:
                e.setMarker( g.MARKER_BOUND_HOMOGEN_NEUMANN )

        boundary = ( self.paraXLimits[ 1 ] - self.paraXLimits[ 0 ] ) * 2.0
        boundaryDomain = []
        if top == 0.0: # assuming flat earth
            boundaryDomain.append( paraDomain[ 0 ] )
            boundaryDomain.append( self.poly_.createNode( paraDomain[ 0 ].pos() - g.RVector3( boundary, 0.0 ) ) )
            boundaryDomain.append( self.poly_.createNode( paraDomain[ 0 ].pos() - g.RVector3( boundary, boundary ) ) )
            boundaryDomain.append( self.poly_.createNode( paraDomain[ -1 ].pos() + g.RVector3( boundary, -boundary ) ) )
            boundaryDomain.append( self.poly_.createNode( paraDomain[ -1 ].pos() + g.RVector3( boundary, 0.0 ) ) )
            boundaryDomain.append( paraDomain[ -1 ] )
        else:
            boundaryDomain.append( self.poly_.createNode( g.RVector3( paraDomain[ 0 ].pos()[ 0 ] - boundary, 0.0 ) ) )
            boundaryDomain.append( self.poly_.createNode( paraDomain[ 1 ].pos() - g.RVector3( boundary, boundary ) ) )
            boundaryDomain.append( self.poly_.createNode( paraDomain[ 2 ].pos() + g.RVector3( boundary, -boundary ) ) )
            boundaryDomain.append( self.poly_.createNode( g.RVector3( paraDomain[ 3 ].pos()[ 0 ] + boundary, 0.0 ) ) )

        for i, n in enumerate( boundaryDomain ):
            e = self.poly_.createEdge( n, boundaryDomain[ (i + 1) % len(boundaryDomain) ], g.MARKER_BOUND_MIXED )

            if e.shape().center()[1] == 0.0:
                e.setMarker( g.MARKER_BOUND_HOMOGEN_NEUMANN )

            if i == len( boundaryDomain )-1 and top == 0.0:
                break

        
        self.poly_.addRegionMarker( g.RVector3( paraDomain[ 0 ].pos() - g.RVector3( 1, 1 ) ), 0, 0.0 )
        self.poly_.addRegionMarker( g.RVector3( paraDomain[ 0 ].pos() + g.RVector3( 1, -1 ) ), 1, 0.0 )
        tri = g.TriangleWrapper( self.poly_ );
        tri.setSwitches( "-pzeAfaq34.4" );
        tri.generate( self.mesh );

        #if self.smoothProp():
        self.mesh.smooth( 1, 1, 0, 4 )
        self.mesh.save( 'mesh.bms' )
        print("ParaMesh", self.mesh)

    def drawData_( self ):
        """ Define what we have to be drawn (needed from base class). Is called while a draw event is fired """
        
        if self.mesh:
            drawMesh( self.axes, self.mesh )
            drawSensors( self.axes, self.sensors )

            eSpacing = self.sensors[ 0 ].distance( self.sensors[ 1 ] )
            self.axes.set_xlim( self.sensors[ 0 ].x() - 4.0 * eSpacing, 
                                self.sensors[ -1 ].x() + 4.0 * eSpacing )
            self.axes.set_ylim( -self.paraDepthProp() - eSpacing, eSpacing/2.0 )
    #def drawData_( ... )
            
            