# -*- coding: utf-8 -*-

import wx

import pygimli as g

from pygimli.gui.wxmpl import AppResourceWxMPL
from pygimli.mplviewer import *

class BertModelHandler( AppResourceWxMPL ):
    def __init__( self, parent, rendererSlot, propertyInspectorSlot ):
        AppResourceWxMPL.__init__( self, parent, rendererSlot, propertyInspectorSlot )
        self.setName( "Model" )
        self.piName_    = "Model"
        self.mesh_      = None
        self.values_    = None
        self.gci_       = None
        self.isNewMesh  = True

    def setMesh( self, mesh ):
        self.mesh_ = mesh
        self.isNewMesh = True
        print("setData::draw")
        self.draw()

    def setModel( self, model ):
        self.values_ = model

        if self.values_ is not None and self.gci_ is not None:
            g.mplviewer.setMappableData( self.gci_, self.values_, cMin = None, cMax = None, logScale = True )
            
        self.updateDrawOnIdle()

    def drawData_( self ):
        ''
        ' Define what we have to be drawn (needed from base class) is called while a draw event is fired '
        ''
       
        if self.mesh_ is not None:
            self.gci_ = g.mplviewer.drawModel( self.axes, self.mesh_ )

        if self.values_ is not None and self.gci_ is not None:
            g.mplviewer.setMappableData( self.gci_, self.values_, cMin = None, cMax = None, logScale = True )

    # def drawData_( ... )
