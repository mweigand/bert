# -*- coding: utf-8 -*-
"""
Setup script for pygimli and python apps.

Usage:

    python setup.py install --user # for local installation
    sudo python setup.py install # for system-wide installation
    python setup.py develop # for developers (creates symlinks to source)

"""

import os
from setuptools import setup, find_packages

# Including Python apps produces weird encoding error
#apps = ["apps/" + app for app in os.listdir('apps') if not "template" in app]
apps = []

try:
    # fails with py3 due to ascii encoding problem.
    with open(os.path.join("../README.md")) as f:
        long_description = f.read()
except:
    long_description = "Boundless Electrical Resistivity Tomography"

setup(name="pybert",
      version="2.2.6",
      description="Boundless Electrical Resistivity Tomography",
      long_description=long_description,
      author="Carsten Rücker, Thomas Günther",
      author_email="mail@pygimli.org",
      url="http://www.pygimli.org",
      packages=find_packages(),
      package_data={'': ['*.so']},
      scripts=apps)
